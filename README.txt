# ------------------ JET DATA COLLECTOIN AND FITTING SUITE ----------------- #
#                           WILLIAM GURECKY: FEB2013                         #
#                                                                            #
# Provides tools to organize radial pressure distribution data from a CFD or #
# custom source.  Provides tools to compute moments of the radial pressure   #
# distributions.  Rebuild the jet's pressure field from moment information.  #
#                                                                            #
# WLG JUNE2013:  Added tree-like data storage class.  Added coefficient      #
# t-testing. Added capability to handle peicewise regression surfaces.       #
#                                                                            #
# WLG SEP2013: Begin Refactor with Davis Ballew.                             #
#   > Clean up variable names (avoid underscores)                            #
#   > convert lists to numpy arrays where possible                           #
#   > rewrite peicewise surface fitting routines to improve code clarity     #
#   > create unit tests for core compoents                                   #
#   > enable user to save / load a previous JetData class so the surface     #
#   fits do not have to be recomputed                                        #
#   > add ability to supply a text based input file (for MATLAB integration) #
#   > output ZOI volumes, fitting params, ect. to txt file                   #
#   > eliminate dependence on non-standard playdoh module                    #
#   > improve jet boundary detection algorithm                               #
#   > improve moment computations, ensure correctness                        #
#   > Base "best" surface fit off of something other than just R^2           #
#                                                                            #
# WLG JUN2014: Implement Uncertainty Propogation                             #
#   > Uncertainty propogation only availible for the "simple" pressure       #
#     profile fitting method                                                 #
#   > A number of new settings availible                                     #
#   > Optional extrapolation protection to prevent model from being abused   #
# -------------------------------------------------------------------------- #
