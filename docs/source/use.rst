Usage
=====

Primary usage overview and key terms. List of input and output.


.. _collect_data:

Collecting Data
---------------

The data used to generate the reduced order model can be collected in various ways.  
The data can be generated via an existing pressure predicting methodology such as the one described in [Kastner]_, collected experimentally using measured 
pressure profiles of impinging jets, or generated using a Computational Fluid Dynamics (CFD) software package.
For the reduced order modeling program to process the data, it must be stored in directories with the structure shown below:

.. image:: images/filelayout.png
    :scale: 80 %
    :align: center

Inputs
------
There are several user inputs that need to be collected before developing the reduced order models.  
To collect these, the user must provide a text file defining the parameters shown in the following table.  Many of these parameters take on default values if not supplied.

.. table:: Input Table

    =====================   =====================================================================================  ==============================================================================
     Input Parameter                    Description                                                                 Sample Input in Text File
    =====================   =====================================================================================  ==============================================================================
    rootDir                 Directory in which data is stored                                                      ``rootDir : /home/user/data/``
    tagP0                   RegEx to find upstream pressure tag in .sum file                                       ``tagP0 : \s*Upstream\sPressure\s+({\d.e+-]+)``
    tagT0                   RegEx to find upstream temperature tag in .sum file                                    ``tagT0 : \s*Upstream\sTemperature\s+({\d.e+-]+)``
    fileTag                 Data file filename tag preceding normalized distance from break                         ``fileTag : ld_``
    outdir                  Directory that will be created to hold results                                          ``outdir: ./Results/``
    cutoffP                 Jet boundary threshold pressure (Pa).                                                   ``cutoffP : 105000``
    maxM                    Maximum number of moments to be calculated                                              ``maxM : 4``
    dataD                   Diameter of break used in data collection (m)                                           ``dataD : 0.167``
    dimlessR                Boolean identifying if data files provided have dimensionless radii (1) or not (0)      ``dimlessR : 0``
    dimlessL                Boolean identifying if data files provided have dimensionless length (1) or not (0)     ``dimlessL : 1``
    ln                      Boolean identifying use of natural log of pressure data (1) or not (0)                  ``ln : 1``
    method                  Pressure profile curve fitting methodology to use                                       ``method : legendre``
    dmgP                    List of pressures for which zones of influence will be calculated (Pa)                  ``dmgP : 105000, 500000, 1000000, 50000000``
    P0                      Upstream pressure for which damage potential will be predicted (Pa)                     ``P0: 8000000``
    T0                      Upstream temperature for which damage potential will be predicted (Pa)                  ``T0 : 500``
    setMaxBetaSkip          Maximum number of fitting coefficients to remove from parent                            ``setMaxBetaSkip : 2``
    tSigLvl                 Two tailed significance level used in t-testing                                         ``tSigLvl : 0.1``
    fidelity                Boolean. (1) multiple dmgP - fitted profile intersections accounted for.                ``fidelity : 0``
    judgeFit                Specify statistic used to determine best fit surface (BIC, AIC, rsqrd, Fratio)          ``judgeFit : BIC``
    piecewise               Boolean.  (1) allows piecewise surface regression. (0) does not                         ``piecewise : 1``
    ldStart                 First L/D location to reconstruct pressure profiles                                     ``ldStart : 0.1``
    ldMax                   Last L/D location to reconstruct pressure profiles                                      ``ldMax : 10``
    ldInc                   L/D step size for reconstruction.  Finer step size for more precise ZOI estimate. [m]    ``ldInc : 0.1``
    extrapolate             Boolean.  (1) allow extrapolation of model. (0) do not                                  ``extrapolate : 0``
    pickleF                 Specifies serialized surface fit data (Pickle file) path                                ``pickleF : /home/user/test/test.pickle``
    =====================   =====================================================================================  ==============================================================================

The defaults (if any) and a recommended setting are provided in the table below:

.. table:: Input Recommendations

    =====================   =====================================================================================  ==============================================================================
     Input Parameter                    Recommendation                                                               Default Value
    =====================   =====================================================================================  ==============================================================================
    rootDir                 Must be user defined. Absolute path required.                                          ``-``
    tagP0                   Highly recommended to define.  Must match string in .sum file                           ``tagP0 : \s*Upstream\sPressure\s+({\d.e+-]+)``
    tagT0                   Highly recommended to define.  Must match string in .sum file                           ``tagT0 : \s*Upstream\sTemperature\s+({\d.e+-]+)``
    fileTag                 Highly recommended to define.  Matches pressure profile file labels                      ``fileTag : ld_``
    outdir                  Must be user defined. Relative path recommended.                                        ``-``
    cutoffP                 Highly recommended input.                                                                ``cutoffP : 105000``
    maxM                    Highly recommended input. ``>=4``                                                        ``maxM : 4``
    dataD                   Must be user defined.                                                                   ``-``
    dimlessR                Must be user defined.                                                                   ``-``
    dimlessL                Must be user defined.                                                                   ``-``
    ln                      Recommended to leave at 1                                                                ``ln : 1``
    method                  Highly recommended to define.                                                            ``method : legendre``
    dmgP                    Must be user defined.                                                                   ``-``
    P0                      Must be user Defined, unless P0 and T0 overrides are supplied via command line input.   ``-``
    T0                      Must be user Defined, unless P0 and T0 overrides are supplied via command line input.   ``-``
    setMaxBetaSkip          Optional.                                                                               ``setMaxBetaSkip : 2``
    tSigLvl                 Optional.                                                                               ``tSigLvl : 0.1``
    fidelity                Optional.  Recommended to leave at 0 for conservative ZOI estimate                        ``fidelity : 0``
    judgeFit                Optional.  Only use Fratio if there is only a single parent to consider                 ``judgeFit : BIC``
    piecewise               Optional.                                                                               ``piecewise : 1``
    ldStart                 Optional.                                                                               ``ldStart : 0.1``
    ldMax                   Optional.  Recommended if the jet truncates                                              ``-``
    ldInc                   Optional.                                                                               ``ldInc : 0.1``
    extrapolate             Recommended to leave at default value.                                                   ``extrapolate : 0``
    pickleF                 Highly recommended to define.  Use a different save path for different ROMs              ``pickleF : ./test.pickle``
    =====================   =====================================================================================  ==============================================================================


This program can be executed from the command line once placed in the appropriate directory (see :ref:`install-label`) like so ::

    $ python2.7 -m jetROM.main -i <inputfile> <options>

The options are detailed below:

:args:
    * -i  <inputfile> - relative or absolute path to the input text file.
    * -o  <outfilepath>  - relative or absolute path to the output file directory.  Can be folder that does not exist yet.
    * -T  <Temperature> - upstream stagnation temperature in Kelvin.
    * -P  <Pressure> - upstream stagnation pressure in Pascals.
    * -D  <Diameter>  - break diameter in meters.

These options are useful if the user wishes to run this module as a standalone
script.

Outputs
-------
The program writes the following files to the user-specified output directory.  All the outputs should be checked to ensure the output ZOI volumes are being computed reasonably.

Zone of Influence Log
+++++++++++++++++++++

The Zone of Influence (ZOI) is the volume for which the jet is at or above a given pressure.  In this case, the given pressure is one of the user-specified pressures in the input text file parameter, ``dmgP``. 
The ZOI log provides the ZOI for each of the pressures in dmgP.  A sample ZOI log is shown in Table 2.

=================  ================
ZOI Vol [L/D]       Dmg P [Pa]
=================  ================
  2.679e+00         1.050e+05
  2.316e+00         1.300e+05
  1.989e+00         2.000e+05
  1.662e+00         3.500e+05
  1.600e+00         4.000e+05
  1.548e+00         4.500e+05
  1.504e+00         5.000e+05
  1.432e+00         6.000e+05
  1.377e+00         7.000e+05
  1.334e+00         8.000e+05
  1.298e+00         9.000e+05
  1.269e+00         1.000e+06
  1.243e+00         1.100e+06
  1.221e+00         1.200e+06
  1.166e+00         1.500e+06
  1.016e+00         3.000e+06
  8.796e-01         6.000e+06
=================  ================

Pressure Profile Log
+++++++++++++++++++++

The pressure profile log outputs the predicted radial pressure profile for the specified upstream pressure and temperature at various normalized lengths from the break plane. 
For example, if the user specifies that the program predict the behavior of a jet with upstream pressure of 8 MPa and 500 K, the pressure profile log will output 
the predicted radial pressure profile for a jet with those upstream conditions at 0.1, 0.2, 0.3, 0.4, … L/D (length/break diameter) from the break plane.  The stepsize is governed by the ldStep input.
These pressure profiles will be generated out to the value specified by ldMax, or by default the maximum L/D for which data exists.

Surface Fitting Log
+++++++++++++++++++++

The surface fitting log outputs the coefficients that were used in the fitting equation to model the moments gathered from the data, 
as well as a measure of their statistical significance.

The final entries in the surface fitting log file will contain the fitting coefficients that are actually used.  All preceding entries log attempted fits.  Some of these fits may contain coefficients that equal to 0.  This indicates some terms in the parent model were found to be statistically insignificant by [ref] method.  An excerpt of the final entry in the log file is shown below ::

    ************** VICTORIOUS FIT ***************
    zone (0-nearfield, 1-farfield): 0 group: 6  moment:3
     ==== General-statistics ==== 
    beta: [  1.48252436e+01  -2.51916928e+00   1.38867421e-01  -3.60481193e+01
      -2.14587366e+01   0.00000000e+00   1.76780163e-02   6.36485657e-05
      -3.83054938e-04   0.00000000e+00]
    SE beta: [  3.38824197e-01   6.30182467e-02   3.47278798e-03   7.58217604e-01
       4.21139858e-01   0.00000000e+00   9.75772735e-04   4.33514235e-06
       1.29346432e-05   0.00000000e+00]
    Covar beta: [[  6.97576048e+02  -1.29246950e+02   7.12662437e+00  -1.55877980e+03
       -8.63648970e+02   0.00000000e+00   3.27797405e-02  -2.56232858e-04
       .
       .
       .
    R^2: 0.999835427497
    ChiSqrd: -22433.1105882
    CritChi: 2664.33264263   siglvl (alpha): 0.1
    Fratio: 6070.6286593
    AICc: -22431.1105882
    BIC: -19859.1105882
     ==== T-statistics ==== 
    T statistic (B/SE(B)): [ 43.75497313 -39.97523592  39.98730185 -47.54323707 -50.95394373   0.
      18.11694019  14.68200133 -29.61465059   0.        ]
    T scores (|t| - tcrit): [ 42.47309233  38.69335511  38.70542105  46.26135627  49.67206292
      -1.28188081  16.83505938  13.40012052  28.33276979  -1.28188081]   siglvl (alpha): 0.1
    Confidence interval, B_i +/-: [  4.34332235e-01   8.07818809e-02   4.45170025e-03   9.71944593e-01
       5.39851100e-01   0.00000000e+00   1.25082434e-03   5.55713577e-06
       1.65806708e-05   0.00000000e+00]


.. note::
    T - test based removal of terms is is not effective for nonlinear least squares fitting.  To turn off T - test based removal, set setMaxBetaSkip to 0.  All coefficients from the parent model will be retained.

In this example, the 6th and the 10th coefficient failed the t-test.  It should be noted that the t-testing procedure works sequentially - only ONE term is removed at a time, then the reduced function is fitted again before the process repeats until either all the terms are significant or the maximum number of terms have been removed as set by the setMaxBetaSkip user input parameter.

Notice the first line contains information on "zone" and "moment."  These entries identify where the fit is applicable and what order moment is represented by the surface.


Original vs. Predicted Moment Plots
+++++++++++++++++++++++++++++++++++

The program outputs charts of all of the 1st – nth moments, where n is specified in the input parameter “maxM,” that were calculated using input data, and compares them with the predicted moments developed using the fitting method.  For example, a plot of predicted vs. original moments is shown in Figure 2.

.. figure:: images/moment_1.png
    :align: center
    :scale: 70 %

    The first moment is shown.  This is a measure of the dimensionless thrust of the jet.

The plot is a 2D representation of a 4D object.  All moments from all input data sets are displayed in this plot in blue.  The red marks indicate a fitted surface slice at the supplied upstream conditions.  It is impossible to ascertain the accuracy of the multidimensional fit from this chart alone.  Therefore, a log file containing the errors between the fitted surface and the input moments is saved in the output directory as well.

Jet Boundary
+++++++++++++++++++++

The detected jet boundary, which identifies the radial location at which the user-specified “cutoffP” is reached, is plotted as a function of normalized length.

Damage Contours
+++++++++++++++++++++

The damage contours graphically show the pressure distribution of an axisymmetric cross-section of the jet with contours specifying regions in which the jet’s pressure is above given specified values.  
A sample graph of damage contours is shown in Figure 4.

.. figure::  images/damage_color.png
    :align: center
    :scale: 70 %


Example
-------

Show simple example with an input deck included.  Show results.  Compare
results to un-fitted result.

After collecting data and writing a viable input file (see section collect_data_ for details)
JetROM can be executed.  Example input files and data are provided in the exampleData
folder.  This example will be using jet data generated from Kastner's model.

Examine the input file and reference :ref:`Input Table` if the inputs are unclear.  An example input file is provided. ::

    $ cd /path/to/jetROM
    $ nano kastnerinput.txt

.. note::
    Windows users may need to change all ``/`` characters to ``\``.  It is possible
    to use the "/" character as a folder delimiter in the latest versions of Windows however.

Change the first line of the input file to reflect the absolute path of this
input file.  The input file will not run if this path is wrong.

From a terminal execute ::

    $ python -m jetROM.main -i /provide/path/to/kastnerinput.txt

Allow the program to finish running.

The program will write data to the location specified in the input file.  ::

    $ cd kastnerout

Examine the outputs.

You may override the settings given in the input file by specifying different
command line arguments to the program.  Try the following ::

    $ python -m jetROM.main -i /path/to/kastnerinput.txt -o ./customtest/ -T 555 -P 8e6 -D 0.185

This will construct a jet at 555 [K] and 8e6 [Pa] and write the output to a new
directory, "customtest".  Compare to the previous results.  The ZOI volumes
should be slightly larger.

We can compare the reconstruction results directly against the input data source.  The
following steps will illustrate:

1. Comparing pressure profiles at various L/D
2. Comparing predicted ZOI against the original ZOI

Navigate to the exampleData folder.  Change directory to the kaster folder and
then to the testkastner folder.  Enter the folder labeled ``8000000.0_550``.
Plot the ld_3.05 data set in the program of your choosing.

Navigate to the results directory, ``/path/to/customtest``.  Open the pressure
profile log text file.  Search the document for the pressure profile nearest to LD 3.05.
Plot the predicted profile data set in against the original data.

To compare the original ZOI shape...

A batchmode utility is also provided.  Its intended use is to create ZOI volume vs
upstream P0, T0 heatmaps.  Examine batchrun.py in the jetROM
directory.  There, it is possible to set P0 and T0 arrays that lie within the
state space covered in the input data. JetROM will run for all combinations of
the values in these arrays.  Execute ::

    $ python -m jetROM.batchrun

Note large areas of apparent 0.0 ZOI volume.  By default, extrapolation beyond the input data thermodynamic envelope is not allowed.  Extrapolation results in a warning and exit from JetROM.  In this case, Kastners model is only available for a narrow band of upstream pressures and temperatures.  Do not exceed the
bounds of source data when using this code.


Interfacing with MATLAB
-----------------------

JetROM outputs the file ``ZOI.mat`` to the user specified output directory.  This
file may be read into MATLAB.  One may then plot the damage contor boundaries.
The file contains all predicted pressure profile - damage pressure intersections computed by jetROM in addition to the ZOI
volumes, upstream pressure, temperature and break diameter.  An interpolation procedure can be used to
obtain a smooth representation of the ZOI contours if desired.

The following code MATLAB code runs jetROM, reads ``ZOI.mat`` and plots the results.  ::

    % run jetROM for some break conditions
    % variable s stores exit status of jetROM
    s = system('python -m jetROM.main -i infile.txt -P 1e7 -T 520 -D 0.125')

    % read in .mat file from jetROM
    load('/outputdir/ZOI.mat')

    % plot ZOI shapes
    hold on
    cmap = jet(256);
    for i=1:length(dmgP)
        scaledP = dmgP(i)/max(dmgP);
        color = cmap(floor(scaledP*256),:);
        dmgR = reshape(ZOIbounds(i,1,:), length(ZOIbounds), 1);
        area(LDs, dmgR, 'FaceColor', color)
        plot(LDs, dmgR, 'black', 'linewidth', 2)
    end
    title('Damage Contours')
    xlabel('Axial Distance From Break [L/D]')
    ylabel('Radial Distance From Break [R/D]')

The resulting plot:

.. image:: images/matlabplot.png
    :scale: 50 %
    :align: center

.. [Kastner] Kastern two phase Impinging jet loads
