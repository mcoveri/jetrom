Theory Guide
============

JetROM is a suite of interpolation routines combined with supporting data structures and plotting tools.  This collection of tools, classes and functions, can be seen as framework for reduced order jet modeling.  

The framework is comprised of methods that:

1.  Harvest pressure distribution data from CFD and experimental results.  
2.  Fit a series of models to the data. 
3.  Build jet pressure contours based on the fitted models.  
4.  Integrate the area enclosed by the contours to yield ZOI volumes. 
5.  Provide fitting statistics to quantify uncertainties in the model and to test fit significance.

This guide focuses on steps 2 through 5.

Jet Dimensional Analysis
++++++++++++++++++++++++

The following Buckingham :math:`\Pi` analysis includes key physical variables of interest for a flashing water jet.  Select physical variables, such as pipe roughness, are omitted from the analysis in an effort to keep the number of independent dimensionless groups to a minimum.  Further more, only a single substance, water, is of interest.  This constrains the surface tension and the viscosity in the Webber and Reynolds number respectively, to only depend upon the fluid pressure and temperature. A dimension describing the size of the obstruction is omitted from our analysis at present.

+--------------------------------+-------------------------------------+
| Variable(s)                    |                   Fundamental Units |
+--------------------------------+-------------------------------------+
| :math:`P_{jet}`                | :math:`[ML^{-1}T^{-2}]`             |
| :math:`P_o`, :math:`P_{\infty}`|                                     |
+--------------------------------+-------------------------------------+
| :math:`D,\ L,\  R_b,\ r`       | :math:`[L]`                         |
+--------------------------------+-------------------------------------+
| :math:`T_o,\ T_{v}`            | :math:`[\theta]`                    |
+--------------------------------+-------------------------------------+
| :math:`\rho_{e}`               | :math:`[ML^{3}]`                    |
+--------------------------------+-------------------------------------+
| :math:`\mu_{e}`                | :math:`[ML^{-1}T^{-1}]`             |
+--------------------------------+-------------------------------------+
| :math:`v_e`                    | :math:`[LT^{-1}]`                   |
+--------------------------------+-------------------------------------+
| :math:`\sigma_e`               | :math:`[MT^{-2}]`                   |
+--------------------------------+-------------------------------------+
| :math:`\omega`                 | :math:`[-]`                         |
+--------------------------------+-------------------------------------+

JetROM treats :math:`\Pi_{1,4,5,10}` as the explanatory variables
and :math:`\Pi_{2,3}`, the dimensionless impingement pressure and the dimensionless
jet boundary, as response variables. 

.. math::
    :label: pigrps

    &\Pi_1 = \frac{P_{\infty}}{P_o}  \\
    &\bf{\Pi_2} = \frac{P_{jet}}{P_{\infty}} \\
    &\bf{\Pi_3} = \frac{R_b}{D} \\
    &\Pi_4 = \frac{L}{D} \\
    &\Pi_5 = \frac{T_{v}}{T_o} \\
    &\Pi_6 = \frac{\sigma}{P_oD} = We^{-1} \\
    &\Pi_7 = \frac{\mu_{e}v_{e}}{P_oD} = Re^{-1} \\
    &\Pi_8 = \frac{\rho v_{e}^2}{P_o} \\
    &\Pi_{9} = \omega \\
    &\Pi_{10} = \frac{r}{D}

The empirical modeling approach used in JetROM involves finding meaningful relationships between response and explanatory dimensionless groups. This empirical approach is much simpler than attempting to construct a mechanistic model due to complex underlying physics.

Computing the Moments
+++++++++++++++++++++

Initially, all length and pressure quantities are non dimensionalized as follows:

.. math::
    :label: dimlessR
    
    R = \frac{r}{R_{b}} = \frac{\Pi_{10}}{\Pi_3}

Where :math:`R_b` is the radial location of the jet boundary.  R is the dimensionless distance to the jet boundary.  In the current implementation, :math:`R_b` is found by linear interpolation between bounding jet radii known for select upstream conditions from the experimental data set.

 .. math::
    :label: dimlessP

    \psi = ln(\frac{P_{jet}}{P_{\infty}}) = ln(\Pi_2)

This nonlinear transformation accentuates the lower pressures found near the jet boundary compared to the high central pressure peak.  Additionally, many impinging pressure profiles take on a Gaussian shape.  Computing the natural log conceals this original exponential behavior.  When using jetROM this transformation is optional, but highly recomended.

An axis symmetric pressure profile is shown below. 

.. image:: images/moment1.png
    :scale: 50 %
    :align: center


The nth un-normalized radial moment may be computed by the following.

.. math::
    :label: nthmoment

    m_n = \int_A P_{jet} r^n r drd\phi

The 0th un-normalized moment is, conveniently, the thrust of the jet with units of [Newtons].  The un-normalized moments may be easy to physically interpret, but they are not used in JetROM.  Instead, dimensionless moments are computed as follows:

.. math::
    :label: normmoment

    M_n = \frac{1}{A} \int_A \psi R^n R dR d\phi

since :math:`R \in [0, 1]`,  the cross sectional area of the non dimensionalized jet is always equivalent to the unit circle area of :math:`\pi`.  Furthermore :math:`\psi` is not a function of :math:`\phi`:

.. math::
    :label: normmoment2

    M_n = \frac{2 \pi}{\pi} \int_R \psi R^n R dR = 2 \int_R \psi R^n R dR

These dimensionless moments will be referred to as the computed moments.  They are computed by Simpsons rule integration.  Their interpretation is clouded by taking the natural log of the pressure profile.

The computed moments serve as the primary system response measure.  They are some function of the upstream thermodynamic conditions and distance to the break plane.  Let :math:`\lbrace M_n \rbrace` be a computed moment set for the nth moment.

Our next goal is to find a suitable collection of functions to describe these moments.

Surface Regression
++++++++++++++++++

Let :math:`S` be an unknown function of :math:`\psi, T_0, and \frac{L}{D}` which describes a predicted moment.  This function may have many unknown coefficients :math:`\lbrace \alpha \rbrace`.  The goal then is to minimize the L2 error, :math:`E`, between a set of such functions, :math:`\lbrace S \rbrace`, and the known moment set :math:`\lbrace M \rbrace` with respect to the coefficients, `\lbrace \alpha \rbrace`.

See [ref] for information on how to provide a custom function :math:`S` when using the program.  Multiple model functions can be supplied.  JetROM will attempt to determine which model best fits the moment data as determined by a Goodness of Fit measure.

.. math::

    E = || S - \lbrace M \rbrace ||_2

.. math::
    :label: surffit

    \frac{\partial E}{\partial \lbrace \alpha \rbrace } = 0

The regression procedure is carried out by ODRPACK (Orthogonal Distance Regression PACK).  Each coefficient is tested for statistical significance via a t-test.  Coefficients that fail the t-test can be automatically removed provided the user allows model reduction by setting maxBetaSkip to a value greater than 0.

The fitted surfaces, represented by :math:`\lbrace S \rbrace`, provide the value of a predicted moment set, :math:`\lbrace\hat{M}\rbrace`, given :math:`\psi, T_0` and :math:`\frac{L}{D}`. 

.. math::
    :label: querySurf

    \lbrace\hat{M}\rbrace = \lbrace S \rbrace|_{\left( \psi, T_0, \frac{L}{D} \right)}


T-testing
---------

The standard error of each fitting coefficient, :math:`SE \lbrace \alpha \rbrace` is provided by ODRPACK.

.. math::
    :label: tscore

    t = \frac{\lbrace \alpha \rbrace}{SE \lbrace \alpha \rbrace}

A critical T-value is computed by evaluating a T-distribution with the
appropriate degrees of freedom and user set significance level.  A t test is
carried out as follows.

.. math::
    :label: ttest

    tscore = |t| - tcrit

If a coefficient's t score is positve, the coefficient is accepted as significant and the null hypothisis
is rejected.  If the t score is negative, the coefficient is flagged.  Multiple
coefficients could fail the T-test, however only the biggest offender is removed
from the fit.  The t-test is a measure of a single
coefficients significance.  Removing two or more coefficients cannot be done
with this method because the removal of a coefficient may render a previously
insignificant term meaningful after refitting.

Goodness of Fit
---------------

Several statistics are availible to determine goodness of fit thanks to ODRPACK.

The AIC

The BIC

The Fratio

.. Note::
    The Fratio is only applicable if only one parent function is defined.
    The Fratio is only useful in comparing a reduced model from a full model -
    the reduced model is formulated by removing select coefficients
    from the full model.

Coefficient of determination is also provided by ODRPACK.  

The user of JetROM can choose between these goodness of fit measures when
attempting to determine which surface best captures the trends in the original moments.


Pressure Profile Reconstruction
+++++++++++++++++++++++++++++++

Let :math:`g(R)` be a function we wish to construct pressure profiles with.

Jet re-construction involves minimizing the L2 norm, :math:`E`, between the estimated moments and the moments of a chosen model function, :math:`\lbrace{m_g}\rbrace`, and solving the resulting system of equations to find the model radial pressure profile coefficients.

.. math::
    :label: optimize

    E = || \lbrace m_g \rbrace - \lbrace\hat{M}\rbrace ||_{_2}

:math:`\lbrace\hat{M}\rbrace` are the moments computed from fitted surface equations.  Each moment of the model radial pressure profiles is defined by:

.. math::
    :label: gmoments

    m_{g_n} = 2 \int_R (R)^n g(R) R dR 

This is non linear least squares problem which is solved by Newtons Method.  A good guess for the coefficients in :math:`g` should be supplied.  It is not guaranteed that the combination of coefficients found by this procedure corresponds to the global minimum of E.  To combat this problem, a stochastic global minimization, basin-hopping approach is used on the initial fitting attempt.  Similar to simulated annealing, a good value for the "temperature" should be selected to overcome peaks separating local minima.  Unlike simulated annealing, a cooling schedule is not used.  Instead, a voting procedure is carried out that selects the most arrived at minima as the global minimum after a given number of hops have been preformed.  The initial guess for subsequent pressure profile coefficients farther from the break plane is equivalent to the coefficients of the pressure profile axially preceding the this profile.  This only performs as intended if the pressure profiles do not abruptly change shape as in the case of shocks.

An example for :math:`g` is given below:

.. math::
    :label: exp

    g(R) = \alpha_0 e^{\alpha_1 R^2 + \alpha_2 R^4} + \alpha_3

:math:`\alpha_i` are the fitting coefficients.

This pressure profile function is similar to the pressure profile predicted by [Kastner]_.  It is highly recommended to use this functional form for impinging data sets.


Special Case: Legendre Polynomials
----------------------------------

Utilizing polynomials as a basis for pressure profile reconstruction makes pressure profile reconstruction a linear least squares problem.  This is drastically reduces computation time.  Polynomials of low order cannot reproduce complex pressure profiles, furthermore, as a general rule of thumb attempts to use higher than 4th order polynomials in most cases will yield poor results unless the predicted moments are very well behaved.

Legendre polynomials make are orthogonal on the interval :math:`[-1, 1]`.  The nth Legendre polynomial can be computed from:

.. math::
    :label: legendre

    P_n(x) = \frac{1}{2^n}\sum_{k=0}^N{ {n \choose{k}}^2(x-1)^{n-k}(x+1)^k }

The maximum Legendre polynomial order to compute is :math:`N`. :math:`N \leq`
length of predicted moment vector, :math:`\hat{M}`.

:eq:`transform` takes abscissas on :math:`[0, 1]` to the compatible Legendre interval of
:math:`[-1, 1]`.

.. math::
    :label: transform

    z = 2(R)-1

Computing the non dimensionalized, radial moments of a linear combination of Legendre polynomials is straight forward.  This procedure is detailed in [Volpi]_:

.. math::
    :label: legendreMom

    m_{g_n} = \sum_{i=0}^{N}{w_i\left( \int_{-1}^1{z^nP_i(z) z dz} \right) }

Where w is the weight of the nth Legendre polynomial.

Using the coordinate transform :eq:`transform` on :math:`R` in equation :eq:`normmoment2`, performing the surface fitting and then equating :eq:`querySurf` and :eq:`legendreMom` produces a linear system:

.. math::

    \bf{Aw}=\bf{{\hat{M}}}

The entries in the regression matrix :math:`\bf{A}` are:

.. math::

    &A_{i+1,j+1}=\int_{-1}^1{z^jP_i(z) z dz} \\
    &i = 0...N \\
    &j = 0...N

This system can be solved [:func:`jetROM.jetZOI.solveSys`] for the Legendre polynomial weights, :math:`\{ \bf{w}\}`. The resulting interpolating polynomial is:

.. math::

    g(z) = w_0P_0+ w_1P_1(z) ... + w_NP_N(z)

After after applying the inverse of :eq:`transform` on :math:`g(z)`, the remaining steps remain unchanged.

Computing ZOI Shape and Volume
++++++++++++++++++++++++++++++

After computing the predicted pressure profiles for the L/D of interest, the intersections between the
predicted profiles and a given damage pressure can be computed.  This collection
of intersections define a ZOI boundary at a given damage pressure.

.. figure:: images/intersect.png
    :align: center
    :scale: 70 %

The damage contours are revolved about the central axis and integrated by the trapezoidal rule to compute ZOI
volumes.

.. figure:: images/revVol.png
    :align: center
    :scale: 65 %

The volume equivalent spherical radius can be computed as follows:

.. math::
    :label: eqr

    N_V = \frac{1}{D} \left( {\frac{3V}{4\pi}}\right)^\frac{1}{3} 

:math:`V` is the volume of the ZOI in meters and :math:`N_V` is the normalized volume equivalent radius in break diameters.


.. [Volpi]  Moment regression using Legendre Polynomials

