.. jetROM documentation master file, created by
   sphinx-quickstart on Sun Nov 10 21:01:25 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to jetROM's documentation!
==================================

Contents:

.. toctree::
   :glob:

   intro*
   use*
   theory*
   symbols*
   code*


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

