Glossary of Terms
=================

Key symbols and terms.

:math:`P`
    Pressure.

:math:`D`
    Break diameter.

:math:`L`
    Distance to break plane.

:math:`R`             
    Dimensionless, fractional radial distace to jet boundary.

:math:`r`
    Radial distance.

:math:`T`            
    Temperature.

:math:`\rho`               
    Density.

:math:`\mu`                
    Viscosity.

:math:`v`                    
    Velocity.

:math:`\sigma`               
    Surface tension.

:math:`\omega`                 
    Void fraction.

Subscripts
++++++++++

:math:`_b`
    At jet boundary.

:math:`_e`
    At exit plane.

:math:`_{jet}`
    Impinging or total pressure of jet depending on case.

:math:`_o`
    Upstream stagnation state.

:math:`_v`
    Vapor.


