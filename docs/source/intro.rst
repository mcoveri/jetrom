Introduction
============

The JetROM (Jet Reduced Order Model) package reconstructs a pressure field from
axis-symmetric pressure profile information.  The input data may be obtained from a CFD or experimental source. The data source should have these attributes:

- Axis-symmetry
- Smooth, well behaved radial profiles
- Moderate axial data resolution
- Data for a minimum of 16 unique upstream thermodynamic conditions
- Axis-symmetric break geometry
- Known break diameter

This software was written to fulfill a portion of GSI-191 related work in hopes of improving or replacing the incumbent jet model. In this work, the jet model is used to predict damage potential of a flashing water jet resulting from a LOCA (Loss of Coolant Accident) in a PWR (Pressurized Water Reactor).  Utilizing a reduced order model allows many break scenarios to be evaluated quickly.

The end user of a jet model generated via this package only needs to supply an upstream stagnation state and a break hydraulic diameter to generate ZOIs.  This simplicity makes this method ideal for use as a component model in a LOCA accident analysis code.

This modeling approach provides a consistent basis on which to build an interpolating jet model from many different flashing jet data sets while retaining usability.  The predictive power of the model relies upon the quantity and quality of the parent data source. 

Background
----------

The accident sequence is summarized below:

1. Pipe Rupture.  Stagnation conditions of 100 - 150 [Bar] and 500 - 550 [K].
2. Flashing impinges on nearby obstructions
3. Debris from impingement falls to floor
4. Debris filtered by submersible pump screens
5. Emergency cooling system performance loss

Following accidental pipe rupture, coolant rapidly accelerates due to a large pressure differential. It is likely the initially subcooled fluid reaches saturation near the exit plane.  Rapid phase change begins, with vapor pockets preferentially nucleating on the pipe surface [Henry]_. In the case where the coolant is initially extra-ordinarily subcooled, the fluid may reach the break plane before an appreciable amount of vapor is formed [Leung]_.  It is hypothesized that these highly subcooled conditions give rise to a liquid core extending beyond the break plane.  In this region, no significant loss in total pressure is experienced.  

According to [HF]_, combining continuity (per phase) and momentum conservation equations across the section of pipe leading to the break plane provides expressions for the critical mass flux.

For the upstream conditions of interest, complex compressible flow features may be present in the jet.  If a bow shock is present upstream of the obstruction, the static pressure and static temperature across the shock sharply rise and the velocity falls.  An overall net loss in total pressure is experienced across a shock.

A typical free and impinging jet are shown below.  Either case is tractable to this package, however, the free jet case typically exhibits more compressible behavior and is thus more difficult to reconstruct accurately.

.. image:: images/freeJet.png
    :scale: 50 %
    :align: center

.. image:: images/impJet.png
    :scale: 50 %
    :align: center

The jet boundary is defined to be the location where the total pressure falls
below :math:`1.01 P_{atm}`.


.. [Henry] Henry 1970 critical flow model
.. [HF] Henry and Fauske critical flow model
.. [Leung] Leung critical flow model

.. _install-label:

Installation Instructions
=========================

JetROM is Unix and Windows compatible.  It is recommended that the target machine has
4 or more cores to take advantage of simple parallel optimization capabilities
of JetROM.

The code is still considered "unstable" and in development.  Therefore, the
following install procedure (and only install procedure available at this time)
is for a developer install.  No pre-built binary exists.

Pre-requisites:

- python2.7
- numpy ``>=1.7.0``
- scipy ``>=0.12.0``
- matplotlib ``>=1.1``

Linux
------

If not already present, make a python project directory to house this project and all future python projects ::

    $ mkdir mypython
    $ cd mypython

Clone repository from git ::
    
    $ git clone www.bitbucket.org/mcoveri/jetrom.git
    $ cd jetrom

Add the project directory to your ``PYTHONPATH`` env variable.  ::
    
    $ echo `export PYTHONPATH=$PYTHONPATH:/path/to/mypython` >> /home/user/.bashrc

Run the install script ::

    $ python setup.py install 

or if you wish to make modifications to the source ::

    $ python setup.py develop


    
Windows
-------

Same as for linux with the appropriate powershell commands in place of bash.
Ensure that the install location is in PYTHONPATH.

To install the pre-requisites, one can simply install the WinPython portable
Python distribution.  This software contains Python 2.7, an IDE, numpy and scipy packages.  

.. Note::
    If WinPython is installed the ``PYTHONPATH`` variable is set in the
    ``WinPython\settings\winpython.ini`` file.
