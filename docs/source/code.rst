Documentation of Code
=====================

Key functions and classes along with their inputs and outputs are detailed here.  Not all functions are included in this documentation.

Individual Section Data
***********************


Pressure Profile Reconstruction
*******************************

.. automodule:: jetROM.jetZOI
    :members: legReg, solveSys, evalLegendre


Compute ZOI
***********

.. automodule:: jetROM.jetZOI
    :members: dmgR
