import scipy.interpolate as interp
import numpy as np

''' Simple saturated h20 properties,
    valid from 1 [bar] to 220 [bar].
    By: William Gurecky
    Feb 14 2013 '''


class satTable(object):
    def __init__(self, infile='h20.txt'):
        ''' Build data arrays for interpolation
        '''
        self.stData = np.genfromtxt(infile)
        self.temp_P = interp.interp1d(
            self.stData[:, 1], self.stData[:, 0])
        self.pres_T = interp.interp1d(
            self.stData[:, 0], self.stData[:, 1])
        self.mycvl_P = interp.interp1d(
            self.stData[:, 1], self.stData[:, 12])
        self.myrhol_P = interp.interp1d(
            self.stData[:, 1], self.stData[:, 2])
        self.myrhov_P = interp.interp1d(
            self.stData[:, 1], self.stData[:, 3])
        self.mysvoll_P = interp.interp1d(
            self.stData[:, 1], self.stData[:, 4])
        self.mysvolv_P = interp.interp1d(
            self.stData[:, 1], self.stData[:, 5])
        self.myul_P = interp.interp1d(
            self.stData[:, 1], self.stData[:, 6])
        self.myuv_P = interp.interp1d(
            self.stData[:, 1], self.stData[:, 7])
        self.myhl_P = interp.interp1d(
            self.stData[:, 1], self.stData[:, 8])
        self.myhv_P = interp.interp1d(
            self.stData[:, 1], self.stData[:, 9])
        self.mysl_P = interp.interp1d(
            self.stData[:, 1], self.stData[:, 10])
        self.mysv_P = interp.interp1d(
            self.stData[:, 1], self.stData[:, 11])
        self.mycvv_P = interp.interp1d(
            self.stData[:, 1], self.stData[:, 13])
        self.mycpl_P = interp.interp1d(
            self.stData[:, 1], self.stData[:, 14])
        self.mycpv_P = interp.interp1d(
            self.stData[:, 1], self.stData[:, 15])

    def T_P(self, P):
        return self.temp_P(P)

    def P_T(self, T):
        return self.pres_T(T)

    def rhol_P(self, P):
        return self.myrhol_P(P)

    def rhov_P(self, P):
        return self.myrhov_P(P)

    def voll_P(self, P):
        return self.mysvoll_P(P)

    def volv_P(self, P):
        return self.mysvolv_P(P)

    def ul_P(self, P):
        return self.myul_P(P)

    def uv_P(self, P):
        return self.myuv_P(P)

    def hl_P(self, P):
        return self.myhl_P(P)

    def hv_P(self, P):
        return self.myhv_P(P)

    def sl_P(self, P):
        return self.mysl_P(P)

    def sv_P(self, P):
        return self.mysv_P(P)

    def cvl_P(self, P):
        return self.mycvl_P(P)

    def cvv_P(self, P):
        return self.mycvv_P(P)

    def cpl_P(self, P):
        return self.mycpl_P(P)

    def cpv_P(self, P):
        return self.mycpv_P(P)

    def printdata(self):
        print self.stData
