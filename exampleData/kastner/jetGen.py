# Generates pressure profiles for use in testing
# the jetROM suite.
#
# William Gurecky
#

import itertools
import numpy as np
import steamtbl as steam
import os
import pylab as pl


class pressureDistro(object):

    def __init__(self, P0, T0, **kwargs):
        rmax = kwargs.pop('rmax', 1.5)  # [m]
        Lmax = kwargs.pop('LDmax', 1)  # [m]
        self.D = kwargs.pop('D', 0.157)  # break D [m]
        rstep = kwargs.pop('rstep', 0.001)
        Lstep = kwargs.pop('LDstep', 0.01)
        self.fi = kwargs.pop('phi', 1.1)  # dimless subcool
        self.pinf = kwargs.pop('pinf', 1.01e5)
        self.L = np.arange(0.01, Lmax, Lstep)
        self.r = np.arange(0, rmax, rstep)
        self.LDs = self.L / self.D
        self.rD = self.r / self.D
        self.P0 = P0
        self.T0 = T0

    def expDistro(self):
        ''' This function will generate pressure distributions
        according to a1 *exp(- a2 * x^2 + b2 * x^4). '''
        pass

    def kastner(self):
        ''' valid only out to LD <= 7 or so '''
        fi = self.fi  # dimless subcool
        Pb = self.P0
        Pinf = self.pinf
        lossC = 4.0
        dataDict = {}
        for zD in self.LDs:
            N = np.tanh((0.2 * (zD - 1))) ** 2
            M = np.log(0.65 * np.exp(-0.1193 * lossC ** 0.603) * zD ** -2.21)
            eta = np.exp(-N * fi + (2.54 * N + 3.31 * M) * fi ** 3 - (1.54 * N + 2.31 * M) * fi ** 4.3)
            pCenter = (Pb - Pinf) * eta + Pinf
            # modified kastner WG
            pCenter = (Pb - Pinf) * np.exp(-0.8 * zD - 0.2 * fi) + Pinf
            if self.fi <= 1.0:
                C = 1.4 - 0.4 * self.fi
                # modified kastner WG.
                C = 1.3 - 1.2 * np.sqrt(zD) * np.exp(-0.4 * zD) - 0.1 * self.fi
            else:
                C = (1.938 - 0.99426 * (zD - 0.25) - 0.73559 * (zD - 0.25) ** 2 + 0.85303 * (zD - 0.25) ** 3
                    - 0.31809 * (zD - 0.25) ** 4 + 0.055679 * (zD - 0.25) ** 5 -
                    0.00458193 * (zD - 0.25) ** 6 + 0.000141356 * (zD - 0.25) ** 7)
            Psi = np.exp(-C * self.rD ** 2)
            pressure = (pCenter - Pinf) * Psi + Pinf
            dataDict[zD] = np.array([self.r, pressure]).T
        self.dataDict = dataDict

    def polyDistro(self):
        ''' This function will generate pressure distributions
        according to a polynomial function of order n '''
        pass

    def writeData(self, outpath):
        ''' Writes the contents of dataDict to files '''
        if os.path.exists(outpath):
            print("Warning, jet data files may be overwritten")
        else:
            os.makedirs(outpath)

        with open(outpath + "summary.sum", 'w') as sumOut:
            sumOut.write("Maximum Absolute Pressure  " + str(self.P0) + "\n")
            sumOut.write("Total Temperature (k)  " + str(self.T0) + "\n")

        for zD in self.LDs:
            with open(outpath + "ld_" + str(zD), 'w') as ldOut:
                for row in self.dataDict[zD]:
                    # write r, p cols to file
                    ldOut.write(str(row[0]) + "    " + str(row[1]) + "\n")

    def plotDistro(self, plotpath, zD):
        ''' plot pressure distribution(s) '''
        pl.plot(self.dataDict[zD][0], self.dataDict[zD][1])
        pl.ylabel('Pressure [Pa]')
        pl.xlabel('radius [m]')
        pl.savefig(plotpath + 'zD_' + zD + '.png')


if __name__ == '__main__':
    listP0 = [0.5e7, 0.55e7, 0.6e7, 0.65e7, 0.7e7, 0.75e7, 0.8e7]
    listT0 = [520, 530, 535, 540, 545, 550, 555, 560]
    mash = np.array([listP0, listT0])
    P0T0 = list(itertools.product(*mash))
    for i, P0T0 in enumerate(P0T0):
        P, T = P0T0[0], P0T0[1]
        st = steam.satTable()
        Psat = st.P_T(T)
        fi = Psat / P
        jetData = pressureDistro(P, T, phi=fi)
        if fi > 0.7 and fi <= 1.00:
            jetData.kastner()
            jetData.writeData('./testkastner/' + str(P) + '_' + str(T) + '/')
        else:
            print("warning, current kastner model limited to 0.7<phi<1.0, phi =" + str(fi))
