% run jetROM for some break conditions
% ....

% read in .mat file form jetROM
load('/home/hermes/programs/jetrom/exampleData/ANSIoutSimple/ZOI.mat')
%load('/home/hermes/programs/jetrom/exampleData/kastnerout/ZOI.mat')

% plot ZOI shapes
hold on
cmap = jet(256);
for i=1:length(dmgP)
    scaledP = dmgP(i)/max(dmgP);
    %color = cmap(floor(scaledP*256),:);
    dmgR = reshape(ZOIbounds(i,1,:), length(ZOIbounds), 1);
    plot(LDs, dmgR, 'black', 'linewidth', 2)
    if sum(ZOIupbounds(i,1,:)) ~= sum(ZOIbounds(i,1,:))
        dmgRup = reshape(ZOIupbounds(i,1,:), length(ZOIupbounds), 1);
        dmgRdwn = reshape(ZOIlowbounds(i,1,:), length(ZOIlowbounds), 1);
        plot(LDs, dmgRup, '-r', 'linewidth', 2)
        plot(LDs, dmgRdwn, '-b', 'linewidth', 2)
    end 
end
title('Damage Contours')
xlabel('Axial Distance From Break [L/D]')
ylabel('Radial Distance From Break [R/D]')