
FLUENT
Version: axi, dp, pbns, mixture, sstkw, transient (axi, double precision, pressure-based, Mixture, SST k-omega, transient)
Release: 14.0.0
Title: 

Models
------

   Model                        Settings                       
   ---------------------------------------------------------
   Space                        Axisymmetric                   
   Time                         Unsteady, 1st-Order Implicit   
   Viscous                      SST k-omega turbulence model   
   Heat Transfer                Enabled                        
   Solidification and Melting   Disabled                       
   Radiation                    None                           
   Species                      Disabled                       
   Coupled Dispersed Phase      Disabled                       
   NOx Pollutants               Disabled                       
   SOx Pollutants               Disabled                       
   Soot                         Disabled                       
   Mercury Pollutants           Disabled                       

Material Properties
-------------------

   Material: water-vapor (fluid)

      Property                        Units      Method       Value(s)                                                                                                                                                  
      ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
      Density                         kg/m3      ideal-gas    #f                                                                                                                                                        
      Cp (Specific Heat)              j/kg-k     polynomial   (300-1000: 1563.0767 1.6037546 -0.0029327841 3.2161009e-06 -1.1568268e-09) (1000-5000: 1233.2338 1.4105233 -0.00040291411 5.5427718e-08 -2.949824e-12)    
      Thermal Conductivity            w/m-k      constant     0.0261                                                                                                                                                    
      Viscosity                       kg/m-s     constant     1.34e-05                                                                                                                                                  
      Molecular Weight                kg/kgmol   constant     18.01534                                                                                                                                                  
      Standard State Enthalpy         j/kgmol    constant     -2.418379e+08                                                                                                                                             
      Reference Temperature           k          constant     298.15                                                                                                                                                    
      Thermal Expansion Coefficient   1/k        constant     0                                                                                                                                                         
      Speed of Sound                  m/s        none         #f                                                                                                                                                        

   Material: water-liquid (fluid)

      Property                        Units      Method     Value(s)         
      --------------------------------------------------------------------
      Density                         kg/m3      constant   998.2            
      Cp (Specific Heat)              j/kg-k     constant   4182             
      Thermal Conductivity            w/m-k      constant   0.6              
      Viscosity                       kg/m-s     constant   0.001003         
      Molecular Weight                kg/kgmol   constant   18.0152          
      Standard State Enthalpy         j/kgmol    constant   -2.8584122e+08   
      Reference Temperature           k          constant   298              
      Thermal Expansion Coefficient   1/k        constant   0                
      Speed of Sound                  m/s        none       #f               

   Material: air (fluid)

      Property                        Units      Method     Value(s)     
      ----------------------------------------------------------------
      Density                         kg/m3      constant   1.225        
      Cp (Specific Heat)              j/kg-k     constant   1006.43      
      Thermal Conductivity            w/m-k      constant   0.0242       
      Viscosity                       kg/m-s     constant   1.7894e-05   
      Molecular Weight                kg/kgmol   constant   28.966       
      Standard State Enthalpy         j/kgmol    constant   0            
      Reference Temperature           k          constant   298.15       
      Thermal Expansion Coefficient   1/k        constant   0            
      Speed of Sound                  m/s        none       #f           

   Material: aluminum (solid)

      Property               Units    Method     Value(s)   
      ---------------------------------------------------
      Density                kg/m3    constant   2719       
      Cp (Specific Heat)     j/kg-k   constant   871        
      Thermal Conductivity   w/m-k    constant   202.4      

Cell Zone Conditions
--------------------

   Zones

      name           id   type    
      -------------------------
      surface_body   2    fluid   

   Setup Conditions

      surface_body

         Condition                                              Value         
         ------------------------------------------------------------------
         Material Name                                          water-vapor   
         Specify source terms?                                  no            
         Source Terms                                           ()            
         Specify fixed values?                                  no            
         Fixed Values                                           ()            
         Frame Motion?                                          no            
         Relative To Cell Zone                                  -1            
         Reference Frame Rotation Speed (rad/s)                 0             
         Reference Frame X-Velocity Of Zone (m/s)               0             
         Reference Frame Y-Velocity Of Zone (m/s)               0             
         Reference Frame User Defined Zone Motion Function      none          
         Mesh Motion?                                           no            
         Relative To Cell Zone                                  -1            
         Moving Mesh Rotation Speed (rad/s)                     0             
         Moving Mesh X-Velocity Of Zone (m/s)                   0             
         Moving Mesh Y-Velocity Of Zone (m/s)                   0             
         Moving Mesh User Defined Zone Motion Function          none          
         Deactivated Thread                                     no            
         Laminar zone?                                          no            
         Set Turbulent Viscosity to zero within laminar zone?   yes           
         Embedded Subgrid-Scale Model                           0             
         Momentum Spatial Discretization                        0             
         Cwale                                                  0.325         
         Cs                                                     0.1           
         Porous zone?                                           no            
         Porosity                                               1             
         Equilibrium Thermal Model (if no, Non-Equilibrium)?    yes           
         Non-Equilibrium Thermal Model?                         no            
         Solid Material Name                                    aluminum      
         Interfacial Area Density (1/m)                         1             
         Heat Transfer Coefficient (w/m2-k)                     1             

Boundary Conditions
-------------------

   Zones

      name     id   type              
      -----------------------------
      inlet    8    mass-flow-inlet   
      axis     5    axis              
      outlet   6    pressure-outlet   
      wall     7    wall              

   Setup Conditions

      inlet

         Condition                                    Value    
         ---------------------------------------------------
         Reference Frame                              0        
         Upstream Torque Integral (n-m)               1        
         Upstream Total Enthalpy Integral (w/m2)      1        
         Total Temperature (k)                        520      
         Supersonic/Initial Gauge Pressure (pascal)   101000   
         Direction Specification Method               1        
         Coordinate System                            0        
         Axial-Component of Flow Direction            1        
         Radial-Component of Flow Direction           0        
         X-Component of Flow Direction                1        
         Y-Component of Flow Direction                0        
         X-Component of Axis Direction                1        
         Y-Component of Axis Direction                0        
         Z-Component of Axis Direction                0        
         X-Coordinate of Axis Origin (m)              0        
         Y-Coordinate of Axis Origin (m)              0        
         Z-Coordinate of Axis Origin (m)              0        
         Turbulent Specification Method               1        
         Turbulent Kinetic Energy (m2/s2)             1        
         Specific Dissipation Rate (1/s)              1        
         Turbulent Intensity (%)                      6        
         Turbulent Length Scale (m)                   0.2      
         Hydraulic Diameter (m)                       1        
         Turbulent Viscosity Ratio                    10       
         is zone used in mixing-plane model?          no       

      axis

         Condition   Value   
         -----------------

      outlet

         Condition                                   Value    
         --------------------------------------------------
         Gauge Pressure (pascal)                     101000   
         Backflow Total Temperature (k)              320      
         Backflow Direction Specification Method     1        
         Turbulent Specification Method              0        
         Backflow Turbulent Kinetic Energy (m2/s2)   1        
         Backflow Specific Dissipation Rate (1/s)    1        
         Backflow Turbulent Intensity (%)            10       
         Backflow Turbulent Length Scale (m)         1        
         Backflow Hydraulic Diameter (m)             1        
         Backflow Turbulent Viscosity Ratio          10       
         is zone used in mixing-plane model?         no       

      wall

         Condition                                            Value      
         -------------------------------------------------------------
         Wall Thickness (m)                                   0          
         Heat Generation Rate (w/m3)                          0          
         Material Name                                        aluminum   
         Thermal BC Type                                      1          
         Temperature (k)                                      300        
         Heat Flux (w/m2)                                     0          
         Convective Heat Transfer Coefficient (w/m2-k)        0          
         Free Stream Temperature (k)                          300        
         Wall Motion                                          0          
         Shear Boundary Condition                             0          
         Define wall motion relative to adjacent cell zone?   yes        
         Apply a rotational velocity to this wall?            no         
         Velocity Magnitude (m/s)                             0          
         X-Component of Wall Translation                      1          
         Y-Component of Wall Translation                      0          
         Define wall velocity components?                     no         
         X-Component of Wall Translation (m/s)                0          
         Y-Component of Wall Translation (m/s)                0          
         External Emissivity                                  1          
         External Radiation Temperature (k)                   300        
         Wall Roughness Height (m)                            0          
         Wall Roughness Constant                              0.5        
         Rotation Speed (rad/s)                               0          
         X-component of shear stress (pascal)                 0          
         Y-component of shear stress (pascal)                 0          
         Fslip constant                                       0          
         Eslip constant                                       0          
         Surface tension gradient (n/m-k)                     0          
         Specularity Coefficient                              0          

Solver Settings
---------------

   Equations

      Equation          Solved   
      ------------------------
      Flow              yes      
      Volume Fraction   yes      
      Slip Velocity     yes      
      Turbulence        yes      
      Energy            yes      

   Numerics

      Numeric                         Enabled   
      ---------------------------------------
      Absolute Velocity Formulation   yes       

   Unsteady Calculation Parameters

                                              
      -------------------------------------
      Time Step (s)                   0.001   
      Max. Iterations Per Time Step   240     

   Relaxation

      Variable                    Relaxation Factor   
      ---------------------------------------------
      Pressure                    0.36                
      Density                     0.36                
      Body Forces                 0.36                
      Momentum                    0.36                
      Vaporization Mass           1                   
      Slip Velocity               0.1                 
      Volume Fraction             0.36                
      Turbulent Kinetic Energy    0.36                
      Specific Dissipation Rate   0.36                
      Turbulent Viscosity         0.36                
      Energy                      0.36                

   Linear Solver

                                  Solver     Termination   Residual Reduction   
      Variable                    Type       Criterion     Tolerance            
      -----------------------------------------------------------------------
      Pressure                    V-Cycle    0.1                                
      X-Momentum                  Flexible   0.1           0.7                  
      Y-Momentum                  Flexible   0.1           0.7                  
      Volume Fraction             Flexible   0.1           0.7                  
      Turbulent Kinetic Energy    Flexible   0.1           0.7                  
      Specific Dissipation Rate   Flexible   0.1           0.7                  
      Energy                      Flexible   0.1           0.7                  

   Pressure-Velocity Coupling

      Parameter   Value    
      ------------------
      Type        SIMPLE   

   Discretization Scheme

      Variable                    Scheme               
      ----------------------------------------------
      Pressure                    Standard             
      Density                     First Order Upwind   
      Momentum                    First Order Upwind   
      Volume Fraction             First Order Upwind   
      Turbulent Kinetic Energy    First Order Upwind   
      Specific Dissipation Rate   First Order Upwind   
      Energy                      First Order Upwind   

   Solution Limits

      Quantity                         Limit        
      -------------------------------------------
      Minimum Absolute Pressure        1            
      Maximum Absolute Pressure        13000000     
      Minimum Temperature              290          
      Maximum Temperature              600          
      Minimum Turb. Kinetic Energy     1e-14        
      Minimum Spec. Dissipation Rate   1e-20        
      Maximum Turb. Viscosity Ratio    1000000000   

