# Main jet reconstruction function(s)

import pickle
import re
import warnings
import sys
import os
import argparse
import numpy as np
import jetROM as rf

warnings.simplefilter("ignore")


# Global settings dictionary
ds = {}
args = []


def checkFile(inputFile):
    if not os.path.exists(inputFile):
        sys.exit('Not a vaild input file.')
    return inputFile


def loadROM(ROMFileName):
    ''' Loads ROM file '''
    if os.path.exists(ROMFileName):
        return pickle.load(open(ROMFileName, 'rb'))
    else:
        print('Pickle file not availible yet')
        return None


def makeDir(outdir):
    if os.path.exists(outdir):
        print "Warning: Potentially overwritting previous results."
    else:
        os.makedirs(outdir)


def getDefaults():
    ''' Gives same result as reading an input deck: a large dictionary of
    settings.  This is ONLY useful for testing purposes. '''
    ##################################
    # JetData Initilize input params
    ##################################
    ds['rootDir'] = '/home/hephaestus/programs/jetrom/refactor/kastner/test'
    P0_match = re.compile('\s*Maximum\sAbsolute\sPressure\s+([\d.e+-]+)')
    T0_match = re.compile('\s*Total\sTemperature\s\(k\)\s+([\d.e+-]+)')
    ds['tagP0T0'] = [T0_match, P0_match]
    ds['fileTag'] = 'ld_'
    ds['outdir'] = './test/'
    ##################################
    # Moment prediction settings
    ##################################
    ds['cutoffP'] = 1.05e5  # [Pa] - kastner
    ds['maxM'] = 4
    ds['utilizedM'] = ds['maxM']
    ds['dataD'] = 0.156  # Source data break diameter
    ds['dimlessR'] = False
    ds['dimlessL'] = True
    ds['ln'] = True
    ##################################
    # ZOI initialize params
    ##################################
    ds['pickleF'] = None
    if not 'computeD' in ds:
        ds['computeD'] = ds['dataD']  # Break diameter for ZOI computation
    ds['method'] = 'legendre'
    ds['fidelity'] = 1
    ##################################
    # ZOI building settings
    ##################################
    ds['dmgP'] = np.array([1.05e5, 1.3e5, 2.0e5, 3.5e5, 4e5, 4.5e5, 5e5, 6e5, 7e5, 8e5, 9e5, 1e6, 1.1e6, 1.2e6, 1.5e6, 3e6, 6e6])  # [Pa]
    if not 'P0' in ds and not 'T0' in ds:
        ds['P0'] = 0.61e7
        ds['T0'] = 543.0


def inputConvert():
    ''' convert from str to proper types '''
    floatStrs = ('cutoffP', 'dataD', 'computeD', 'ldMax', 'P0', 'T0', 'ZOIalpha')
    for k in floatStrs:
        if k in ds:
            ds[k] = float(ds[k])
    intStrs = ('maxM', 'utilizedM', 'fidelity', 'extrapolate', 'setMaxBetaSkip')
    for k in intStrs:
        if k in ds:
            ds[k] = int(ds[k])
    boolStrs = ('ln', 'dimlessR', 'dimlessL', 'piecewise')
    for k in boolStrs:
        if k in ds:
            ds[k] = bool(int(ds[k]))
    listStrs = ('dmgP', 0)
    for k in listStrs:
        if k in ds:
            ds[k] = np.fromstring(ds[k], sep=',')
    ds['tagP0T0'] = [re.compile(ds['tagT0']), re.compile(ds['tagP0'])]


def parseInputs(inputFile):
    ''' Try to get inputs from file '''
    getDefaults()  # set defaults first
    # override defaults with parsed inputs
    if inputFile:
        # read text input file
        # np.fromstring(str, sep=',')  will make array from string
        with open(inputFile) as infile:
            for line in infile:
                try:
                    (key, val) = line.split(':')
                    ds[key.strip()] = val.strip()  # assign to input dict and strip whitespace
                except:
                    pass
    checkFile(ds['rootDir'])
    # apply cmd line overrides if provided
    if hasattr(args, 'P') and args.P:
        ds['P0'] = args.P
    if hasattr(args, 'T') and args.T:
        ds['T0'] = args.T
    if hasattr(args, 'D') and args.D:
        ds['computeD'] = args.D
    if hasattr(args, 'o') and args.o:
        ds['outdir'] = args.o
    inputConvert()


def inputConsistCheck(mPredict):
    ''' Check if inputs have changed from run to run when loading
    predicted moment information. '''
    if mPredict.pInf != ds['cutoffP']:
        return None
    elif mPredict.lnswitch != ds['ln']:
        return None
    elif mPredict.momentType != ds['method'] and ds['method'] != 'original':
        return None
    else:
        return mPredict


def main():
    # command line inputs to this script
    parser = argparse.ArgumentParser(description='JetROM - Reduced Order Model Constructor')
    parser.add_argument('-P', type=float, help='Supply P0 [Pa]')
    parser.add_argument('-T', type=float, help='Supply T0 [K]')
    parser.add_argument('-D', type=float, help='Desired break diameter [m]')  # break diameter
    parser.add_argument('-i', type=str, help='Input file')  # input filepath
    parser.add_argument('-o', type=str, help='Output file path')  # output directory
    args = parser.parse_args()
    if args.i:
        inputFile = checkFile(args.i)
    else:
        print('Warning: Attempting to use defaults.  Please supply an input file.')
        inputFile = None
    parseInputs(inputFile)
    makeDir(ds['outdir'])
    if 'pickleF' in ds:
        mPredict = loadROM(ds['pickleF'])
        # check against parseInputs for changes in pinf and lnswitch, we must
        # recompute predicted moments if these fields change
        if mPredict:
            mPredict = inputConsistCheck(mPredict)
    if 'pickleF' not in ds or mPredict is None:
        JetDataInit = {k: ds[k] for k in ('rootDir', 'tagP0T0', 'fileTag', 'outdir') if k in ds}  # easy way to pull subset dict out of large dict
        mPredict = rf.jetData.JetData(**JetDataInit)  # unpack and pass init inputs
        mPredictOpts = {k: ds[k] for k in ('judgeFit', 'tSigLvl', 'method', 'maxM', 'cutoffP', 'ln', 'dataD', 'dimlessL', 'dimlessR', 'setMaxBetaSkip', 'piecewise') if k in ds}
        mPredict.setOpts(**mPredictOpts)  # set options, blank for default opts
        mPredict.build_data()  # gather and normalize data
    jetROMOpts = {k: ds[k] for k in ('method', 'utilizedM', 'profileFn', 'guess', 'ldMax', 'computeD', 'ldStart', 'ldInc', 'fidelity', 'pickleF', 'extrapolate', 'ZOIalpha') if k in ds}
    ROM = rf.jetZOI.JetROM(mPredict, rf.modelBank.equationBank, **jetROMOpts)
    jetROMEval = {k: ds[k] for k in ('P0', 'T0', 'dmgP', 'outdir') if k in ds}
    ROM.constructROM(**jetROMEval)


if __name__ == "__main__":
    ''' __main__ is only executed if this script is run as a standalone program
    from the cmd line.  Otherwise, import jetROM and use it as a module '''
    main()
