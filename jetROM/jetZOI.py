import numpy as np
from scipy import integrate, special
from scipy.optimize import minimize, basinhopping, leastsq
from scipy.interpolate import interp1d
from scipy.stats import norm
import scipy.io as sio
import matplotlib.cm as cm
import matplotlib
import matplotlib.pyplot as plt
import pylab as pl
import math
import sys
import pickle
from numpy import linalg
import functools


class JetROM(object):
    '''
    Jet Reconstruction class.
    Computes and stores ZOI volumes.
    Provides ZOI plotting routines.
    Requires JetData before use.
    '''

    def __init__(self, JetData, fnBank, **kwargs):
        ''' Takes a JetData class as initial input.  Build surfaces. '''
        self.JetData = JetData
        self.setOpts(kwargs)
        if self.maxMoment > self.JetData.maxMoment:
            sys.exit('Number of requested moments exceeds the number of predicted moments availible. Exiting.')
        if not hasattr(self.JetData, 'modelFns'):
            self.JetData.makeSurfaces(self.method, self.maxMoment, fnBank)
            if self.pickleF:
                print("Saving surface fit data to file...")
                saveROM(self.JetData, self.pickleF)
        else:
            print("Compatible moment predictions detected and loaded.  Computing ZOI NOW.")

    def setOpts(self, kwargs):
        ''' Set key ZOI inputs.  Privide sane defaults if field not populated '''
        self.method = kwargs.pop('method', 'legendre')              # pressure profile reconstruction method
        self.maxMoment = kwargs.pop('utilizedM', 4)                 # should not exceed predicted moment surface count
        self.profileModelFn = kwargs.pop('profileFn', [])           # supply custom profile fn
        self.guessCoeffs = kwargs.pop('guess', [])                  # if method is custom, allow user to input initial guess
        self.ldMax = kwargs.pop('ldMax', self.JetData.ldMax - 0.5)  # last axial location at which to reconstruct jet
        self.D = kwargs.pop('computeD', 0.156)                      # break diameter
        self.ldStart = kwargs.pop('ldStart', 0.1)                   # first axial location at which to reconstruct jet
        self.ldInc = kwargs.pop('ldInc', 0.1)                       # jet reconstruction axial step size
        self.pickleF = kwargs.pop('pickleF', None)                  # serialized system state file
        self.fidelity = kwargs.pop('fidelity', 0)                   # attempt to count for multiple intersections?
        self.extrapolate = kwargs.pop('extrapolate', 1)             # prevent extrapolation?
        self.annealT = kwargs.pop('annealT', 4)                     # annealing temperature used in global minimization
        self.uncertFlag = kwargs.pop('uncert', 1)                   # ZOI uncertainty calc flag
        self.ZOIalpha = kwargs.pop('ZOIalpha', 0.95)                # Confidence level of ZOI uncert

    def computeZOI(self):
        ''' compute and plot ZOI '''
        # evalrStar will be dimless radi where intersection occures (between [0, 1])
        self.Rcrit = findRcrit(list(self.evalrStar), self.LDsliceFittedP, self.dmgP, self.LDslices, self.fidelity)
        self.ZOIvols = buildZOIs(self.dmgP, self.Rcrit, list(
            self.LDslices), list(self.jetBounds), self.D, 1.0, self.outdir)  # force Doriginal to 1 if rbound is dimless to begin with
            #self.LDslices), list(self.jetBounds), D, self.JetData.Doriginal, self.outdir)
        if self.method == 'simple' and self.uncertFlag:
            self.RcritHigh, self.RcritLow = self.computeZOIUncert()
            ZOIvolsHigh = buildZOIs(self.dmgP, self.RcritHigh, list(self.LDslices), list(self.jetBounds), self.D, 1.0, self.outdir)
            ZOIvolsLow = buildZOIs(self.dmgP, self.RcritLow, list(self.LDslices), list(self.jetBounds), self.D, 1.0, self.outdir)
            self.ZOInormvolsHigh = volEqRadius(ZOIvolsHigh, self.dmgP, self.D)
            self.ZOInormvolsLow = volEqRadius(ZOIvolsLow, self.dmgP, self.D)
        else:
            # default to 0s for ZOI uncert
            self.RcritHigh = self.Rcrit
            self.RcritLow = self.Rcrit
            self.ZOInormvolsHigh = np.zeros(np.shape(self.ZOIvols))
            self.ZOInormvolsLow = np.zeros(np.shape(self.ZOIvols))
        self.ZOInormvols = volEqRadius(self.ZOIvols, self.dmgP, self.D)
        self.writeZOI()

    def computeZOIUncert(self):
        '''
        Compute uncertainty in ZOI extent.  This is done by finding the uncert
        in the location of all dmgP and fitted pressure profiles intersections at each LD.
        '''
        # only compute Rcrit uncertainties for outer-most intersections
        print("ZOI uncertainty calculations in progress...")
        RcritVar = np.zeros(np.shape(self.Rcrit[:, 0, :]))
        if self.JetData.lnswitch:
            dmgPressures = np.log(self.dmgP) - np.log(self.JetData.pInf)
        else:
            dmgPressures = self.dmgP
        for i, dmgP in enumerate(dmgPressures):
            for j, LD in enumerate(self.LDslices):
                Rcrit = self.Rcrit[i, 0, j]
                RcritVar[i, j] = varRCrit(self.LDsliceExpCoeffs[j], self.LDsliceExpCoeffsSigma[j], self.evalrStar, Rcrit, dmgP)
        sigmaRcrit = np.sqrt(RcritVar)  # compute standard error Rcrits
        RcritHigh1SD = np.minimum(self.Rcrit[:, 0, :] + sigmaRcrit * norm.interval(self.ZOIalpha)[1], 1)  # do not allow Rcrits above jet boundary
        RcritLow1SD = np.maximum(self.Rcrit[:, 0, :] - sigmaRcrit * norm.interval(self.ZOIalpha)[1], 0)  # dont allow negative Rcrit
        print("Completed ZOI Uncert Calcs")
        RcritHigh = np.zeros(np.shape(self.Rcrit))
        RcritLow = np.zeros(np.shape(self.Rcrit))
        RcritHigh[:, 0, :] = RcritHigh1SD
        RcritLow[:, 0, :] = RcritLow1SD
        return RcritHigh, RcritLow

    def writeZOI(self):
        ''' log ZOI to file '''
        headerString = 'ZOI Vol eq Spherical Radius in units of Break Diameters [De] \nDmg P [Pa] | ZOI R [De]   |  +' + str(self.ZOIalpha * 100) + '% CI   /  -' + str(self.ZOIalpha * 100) + '% CI'
        footerString = "Break D [m] = " + str(self.D) + '\n' + "P0 [Pa] = " + str(self.P0) + ", T0 [K] = " + str(self.T0)
        np.savetxt(self.outdir + 'ZOI_log.txt', np.array([self.dmgP, self.ZOInormvols, self.ZOInormvolsHigh, self.ZOInormvolsLow]).T,
                   delimiter='   ', header=headerString, footer=footerString, fmt='%1.6e')
        print(" ZOI Sphere R [De] | Dmg P [Pa]")
        print("================================")
        print(str(np.array([self.ZOInormvols, self.dmgP]).T))
        # save ZOI info to other formats, .mat file for use with MATLAB
        sio.savemat(self.outdir + 'ZOI.mat', {'ZOIbounds': self.Rcrit * np.array(self.jetBounds).T, 'ZOIvols': self.ZOInormvols,
                                              'LDs': self.LDslices, 'dmgP': self.dmgP, 'D': self.D, 'P0': self.P0, 'T0': self.T0,
                                              'ZOIupbounds': self.RcritHigh * np.array(self.jetBounds).T,
                                              'ZOIlowbounds': self.RcritLow * np.array(self.jetBounds).T,
                                              'ZOIhighVol': self.ZOInormvolsHigh, 'ZOIlowVol': self.ZOInormvolsLow,
                                              'jetBounds': np.array(self.jetBounds)})

    def legendre(self):
        ''' Map each row of jetMz (each row correspons to the moments at a
        particular slice) to legendre polynomial coefficients.  This is a
        Linear least squares problem. '''
        self.jetLegendreMoments, self.evalMomentsSigma = self.JetData.evalSurfaces(
            self.LDslices, self.P0, self.T0)
        self.LDsliceLegCoeffs = map(lambda jetMzs: solveSys(legReg(
            self.maxMoment), jetMzs), self.jetLegendreMoments)
        self.LDsliceFittedP = map(lambda sliceCoeffs: evalLegendre(
            self.evalz, sliceCoeffs, self.maxMoment), self.LDsliceLegCoeffs)

    def expharmonic(self):
        ''' Depending on functional form of desired pressure profiles, fitting
        to the predicted moments is either a linear or nonlinear least squares problem.
        The nonlinear least squares problem is hard!  Avoid if possible. '''
        self.normMoments, self.evalMomentsSigma = self.JetData.evalSurfaces(
            self.LDslices, self.P0, self.T0)
        currentGuess = np.array([3, -1, -6, self.pBound])   # TODO: make initial guess for profile coeffs an optional input param
        self.LDsliceExpCoeffs, self.LDsliceExpCoeffsSigma = [], []
        # At first LD slice, global minimization is used (basin hop)
        # Note self.evalMomentsSigma stores the varience in the predicted
        # moments.  This is \sigma_\hat(M) ** 2 , or the standard dev squared.
        expCoeff, coeffSigma = minimizeObjFn(self.normMoments[0], currentGuess, self.maxMoment, self.method, withBasinhop=[1, self.annealT], weights=self.evalMomentsSigma[0])
        self.LDsliceExpCoeffs.append(expCoeff)
        self.LDsliceExpCoeffsSigma.append(coeffSigma)
        for jetMrsChunk, sigmaChunk in zip(self.normMoments[1:], self.evalMomentsSigma[1:]):
            currentGuess = self.LDsliceExpCoeffs[-1]  # set guess to result of last run
            minimizeObjFnPartial = functools.partial(minimizeObjFn, guessCoeffs=currentGuess, n=self.maxMoment, targetFcn=self.method, withBasinhop=[], weights=sigmaChunk)
            pprofileCoeffs, pprofileCoeffsSigma = minimizeObjFnPartial(jetMrsChunk)
            self.LDsliceExpCoeffs.append(pprofileCoeffs)
            self.LDsliceExpCoeffsSigma.append(pprofileCoeffsSigma)
        if self.method == "expharmonic":
            self.LDsliceFittedP = map(lambda sliceCoeffs: momExpFn(
                self.evalrStar, sliceCoeffs, -1), self.LDsliceExpCoeffs)
        elif self.method == "simple":
            self.LDsliceFittedP = map(lambda sliceCoeffs: momSimple(
                self.evalrStar, sliceCoeffs, -1) * np.exp(0.0), self.LDsliceExpCoeffs)
        else:
            self.LDsliceFittedP = map(lambda sliceCoeffs: momTanhFn(
                self.evalrStar, sliceCoeffs, -1), self.LDsliceExpCoeffs)

    def shortCircuit(self):
        ''' Use raw pressure profile data - do NOT use predicted moments -
        Compute ZOI volumes directly from parent profiles. Use nearest neighbor
        procedure if given P0 T0 pair are not in set of parent data P0 T0. '''
        self.LDsliceFittedP = []
        self.LDslices = np.unique(self.JetData.jetLD)
        self.LDslices = self.LDslices[np.where(self.LDslices <= self.ldMax)]
        self.jetBounds = map(lambda LDs: self.JetData.jetRInterp(
                             np.array([[LDs, self.P0, self.T0]])), self.LDslices)
        self.normMoments = self.JetData.evalSurfaces(
            self.LDslices, self.P0, self.T0)
        P0index = np.argmin(np.abs(self.JetData.jetP0 - self.P0))
        T0index = np.argmin(np.abs(self.JetData.jetT0 - self.T0))
        self.P0 = self.JetData.jetP0[P0index]
        self.T0 = self.JetData.jetT0[T0index]
        for jetLD in self.LDslices:
            try:
                originalP = self.JetData.DataTree.getData(['root', jetLD, self.P0, self.T0])[2]
                originalNormR = self.JetData.DataTree.getData(['root', jetLD, self.P0, self.T0])[4]
                originalPdistro = interp1d(originalNormR, originalP)
                self.LDsliceFittedP.append(map(originalPdistro, self.evalrStar))
            except:
                originalP = self.JetData.DataTree.getData(['root', jetLD, self.P0, self.T0])[2]
                originalNormR = self.JetData.DataTree.getData(['root', jetLD, self.P0, self.T0])[4]
                originalPdistro = interp1d(originalNormR, originalP)
                import pdb; pdb.set_trace()  # XXX BREAKPOINT

    def constructROM(self, P0, T0, dmgP, outdir='./test/'):
        ''' Reconstruct pressure profiles '''
        self.outdir = outdir
        self.dmgP = dmgP
        # Normalize and take natural log of pressures if necissary to match source data
        self.P0 = np.log(P0) - np.log(self.JetData.pInf) if self.JetData.lnswitch else P0 / self.JetData.pInf
        self.dmgP = np.log(self.dmgP) - np.log(self.JetData.pInf) if self.JetData.lnswitch else self.dmgP / self.JetData.pInf
        self.pBound = 0.0 if self.JetData.lnswitch else 1.0
        self.T0 = T0
        self.LDslices = np.arange(self.ldStart, self.ldMax, self.ldInc)
        self.evalz = np.arange(-1, 1, 0.005)  # [-1, 1] in increments on 0.005
        self.evalrStar = (self.evalz[:] + 1.0) / 2.0  # [0, 1]
        self.JetData.buildRInterp()
        self.jetBounds = map(lambda LDs: self.JetData.jetRInterp(
                             np.array([[LDs, self.P0, self.T0]])), self.LDslices)
        # prevent extrapolation if extapolation flag is off
        # self.jetBounds will contain 0's if no jetbound data exists at a given
        # (LD, P0, T0) coordinate.  If many 0's are present in self.jetBound,
        # extrapolation is the cause.  See JetData.jetRInterp for more info
        if np.count_nonzero(self.jetBounds) / len(self.jetBounds) < 0.75 and self.extrapolate == 0:
            sys.exit('Extrapolation not permitted.  Set extrapolation flag = 1 to access P0, T0 range outside of parent data source range.')
        print('Reconstruction In Progress.')
        if self.method == 'legendre':
            self.legendre()
        elif self.method == 'expharmonic' or self.method == 'tanh' or self.method == 'simple':
            self.expharmonic()
        else:
            self.shortCircuit()
        if self.JetData.lnswitch:
            # Plotted / output pressures should always be dimensioned [Pa] for
            # ease of interpretation.  exp(ln(P/Pinf)) * Pinf = P
            self.P0 = np.exp(self.P0) * self.JetData.pInf
            self.dmgP = np.exp(self.dmgP) * self.JetData.pInf
            self.LDsliceFittedP = np.exp(self.LDsliceFittedP) * self.JetData.pInf
        else:
            self.P0 = self.P0 * self.JetData.pInf
            self.dmgP = self.dmgP * self.JetData.pInf
            self.LDsliceFittedP = np.array(self.LDsliceFittedP) * self.JetData.pInf
        self.computeZOI()
        self.plotJetBound()
        self.plotSurfaces()
        self.logPressureProfiles()
        self.plotPressureProfile(0.2)

    def plotSurfaces(self):
        ''' Plots predicted moments as a function of P0, T0, LD.
        Projects surface on moment vs LD plane. '''
        for i in range(0, self.maxMoment):
            pl.figure(i + 7)
            if self.method == 'legendre':
                fittedMrs = self.jetLegendreMoments[:, i]
                p1, = pl.plot(self.JetData.jetLD, self.JetData.jetMz[:, i], 'o')
            else:
                fittedMrs = self.normMoments[:, i]
                p1, = pl.plot(self.JetData.jetLD, self.JetData.jetMrs[:, i], 'o')
            p2, = pl.plot(self.LDslices, fittedMrs, 'ro')
            pl.xlabel('LD')
            pl.ylabel('m_' + str(i) + ' moment')
            pl.legend([p1, p2], ['Original', 'Predicted'])
            pl.savefig(self.outdir + 'moment_' + str(i) + '.png')

    def logPressureProfiles(self):
        ''' Writes predicted pressure profile data to file at all LDs '''
        with open(self.outdir + 'PProfile_log.txt', 'w') as outFile:
            outFile.write('   R  [m]   |      P  [Pa]   \n')
            for i, LD in enumerate(self.LDslices):
                outFile.write('LD = ' + str(LD) + '==============\n')
                # evalrStar on [0, 1],  jetBounds dimless R [R/D] * break diam [D] => radial coordinate
                outFile.write(str(np.array([self.evalrStar * self.jetBounds[i] * self.D, self.LDsliceFittedP[i]]).T) + '\n')
                outFile.write("==================================\n")

    def plotPressureProfile(self, LD):
        ''' Plots a predicted pressure profile at the supplied axial location.'''
        pl.figure(4)
        i = np.argmin(np.abs(self.LDslices - LD))
        p2, = pl.plot(self.evalrStar, self.LDsliceFittedP[i], c='black', ls='-', lw=3)
        pl.legend([p2], ['Fitted'])
        pl.title("Predicted Pressure profile at " + str(self.LDslices[i]) + "LD")
        pl.xlabel("Fractional Distance to Jet Boundary")
        pl.ylabel("Jet Pressure [Pa]")
        pl.savefig(self.outdir + str(self.LDslices[i]) + '_' + str(self.P0) + '_' + str(self.T0) + '_pprofile.png')

    def plotJetBound(self):
        ''' Plots the jet boundary vs LD plot for the given upstream conditions '''
        pl.figure(3)
        pl.plot(self.LDslices, self.jetBounds, 'r')
        pl.xlabel('[LD]')
        pl.ylabel('Jet Boundary [R/D]')
        pl.savefig(self.outdir + 'jetbound.png')


################################################################
#                         JET ZOI FN LIBRARY                   #
################################################################

def saveROM(myROM, pickleFile):
    ''' Serialize state of jetData class then write to file. '''
    with open(pickleFile, 'wb') as outfile:
        pickle.dump(myROM, outfile)


def legReg(max_moments):
    '''
    The terms in the output matrix are the moments of each legendre polynomial.

    .. note:: Legendre Polynomials are orthogonal on [-1, 1].

    :param max_moments:  maximum legendre moment
    :type max_moments: int

    :returns:  matrix -- Legendre Vandermonde matrix.
    '''
    L = np.zeros([max_moments, max_moments])
    for n in range(0, max_moments):
        for i in range(0, max_moments):
            L[n, i] = integrate.quad(
                lambda z: z ** (n + 1) * special.eval_legendre(i, z), -1, 1)[0]
    return L


def solveSys(L, C):
    '''
    Find the Legendre polynomial weights.  Solves the system :math:`Lw=C`

    :param L: Moments of the Legendre polynomials.
    :type L: matrix
    :param C: Predicted moments computed from the surface regression
    :type C: array

    :returns: w (array):  Legendre polynomial weights
    '''
    w = np.linalg.lstsq(L, C)[0]
    return w


def evalLegendre(z, w, max_moment):
    '''
    Evaluates Legendre polynomial given input coords, z, on [-1, 1]
    Legendre moment regression procedure adopted from methods described by
    Lenardo Volpi, Foxs Team 2006.

    :param z:  Dimensionless input abscissas
    :type z: array
    :param w: Legendre polynomial weights
    :type w: array
    :param max_moment: maximum legendre moment
    :type max_moment: int

    :returns:  fittedP (array):  ordinate list of fitted pressure profile
    '''
    fittedP = np.zeros(len(z))
    for i, zi in enumerate(z):
        summation = 0
        for n in range(0, max_moment):
            summation = summation + w[n] * special.eval_legendre(n, zi)
        fittedP[i] = summation
    return list(fittedP)


def dmgR(rStar, stagP, dmgP, rCritSwitch):
    '''
    Find where dmgP line intersects prediced pressure profile by
    line search.  Walk from 0 to Rmax untill we encounter intersection.

    :param rStar: Dimensionless Pressure profile abscissas at all L/D slices
    :type rStar: array
    :param stagP: Pressure profile ordinates at all L/D slices
    :type stagP: array
    :param dmgP: Damage pressure
    :type dmgP: float
    :param rCritSwitch: True - attempt to accout for multiple intersections.  False - Only use outermost intersection
    :type rCritSwitch: bool

    :returns:  rCrit (array):  Dimensionless radial location of ZOI bound at all L/D slices
    '''
    intersectionList = []  # Stores all intersection locations
    stagPPairs = np.array((stagP[:-1], stagP[1:])).T
    rStarPairs = np.array((rStar[:-1], rStar[1:])).T
    for pPair, rsPair in zip(stagPPairs, rStarPairs):
        if (dmgP < max(pPair)) and (dmgP >= min(pPair)):
            f = interp1d(pPair[np.argsort(pPair)], rsPair[np.argsort(pPair)])
            intersectionList.append(f(dmgP))
    # only desire the first (outermost) intersection and
    # possibly the 2nd intersection if fidelity set to 1
    rCrit = np.zeros(2)
    if intersectionList:
        rCrit[0] = max(intersectionList)
    if len(intersectionList) > 1 and rCritSwitch:
        rCrit[1] = min(intersectionList)
    if stagP[-1] >= dmgP:
        rCrit[0] = max(rStar)
    return rCrit


def findRcrit(rStar, fittedP, dmgP, LD, rCritSwitch=1):
    '''
    Find the critical radii at all LD and all damage pressures
    relies on dmgR function to find intersections.
    '''
    Rcrit = np.zeros((len(dmgP), 2, len(LD)))
    for i in range(0, len(dmgP)):
        for j in range(0, len(LD)):
            Rcrit[i, :, j] = dmgR(rStar, fittedP[j], dmgP[i], rCritSwitch)
    return Rcrit


def RcritpurtPP(ppCoeffs, dmgP, rStar, rCritGuess):
    '''
    Computes the intersection between a purturbed pressure profile
    and a damage pressure.
    '''
    # only works for 'simple' method right now
    targetProfileFn = functools.partial(momSimple, coeffs=ppCoeffs, N=-1)
    purtPP = targetProfileFn(rStar)
    if rCritGuess == 0:
        #purtRcrit = 0
        purtRcrit = dmgR(rStar, purtPP, dmgP, 0)[0]
    else:
        purtRcrit = dmgR(rStar, purtPP, dmgP, 0)[0]
    return purtRcrit


def varRCrit(ppCoeffs, ppCoeffsCov, rStar, rCrit, dmgP):
    ''' This function computes ||dRcrit/d(alpha) * sigma(alpha)||
    where (alpha) is the set of pressure profile
    coefficients P at the location of the outermost
    intersection. '''
    import common as cmn
    rPP = functools.partial(RcritpurtPP, dmgP=dmgP, rStar=rStar, rCritGuess=rCrit)
    dRcritDbeta = cmn.gradF(rPP, ppCoeffs)
    vRc = np.dot(np.dot(dRcritDbeta, ppCoeffsCov), dRcritDbeta.T)
    return vRc


def plotDmgVolume(LD, dimlessR, pressure_bounds, figname):
    '''
    Plots ZOI contour.
    '''
    color_val = (pressure_bounds[2] - pressure_bounds[0]) / (pressure_bounds[1] - pressure_bounds[0])
    #color_val = np.log(1 + (pressure_bounds[2] - pressure_bounds[0]) / (pressure_bounds[1] - pressure_bounds[0]))
    if color_val < 0:
        color_val = 0
    if color_val > 1:
        color_val = 1
    mycolor = cm.jet(color_val)
    plotDimlessR = dimlessR
    pl.plot(LD, plotDimlessR[0, :], color='black', ls='-')
    pl.plot(LD, plotDimlessR[1, :], color='black', ls='-')
    pl.xlim(0, max(LD))
    pl.fill_between(LD, y1=plotDimlessR[1, :], y2=plotDimlessR[0, :], where=plotDimlessR[0, :] > plotDimlessR[1, :] * 1.25, interpolate=True, color=mycolor, cmap='jet')
    pl.title('Damage Contours')
    pl.xlabel('L/D')
    pl.ylabel('r/D')
    pl.savefig(figname + '.png')


def dmgVolume(LD, Rcrit, rBound, d, dorig, pressure_bounds, fig, out):
    '''
    Take a set of (LD,r) points - defining a damage
    contour - and revolve the resulting enclosed area about the central
    axis. '''
    # Error thrown for ANSI data set:
    LD.insert(0, 0)  # include point at the inlet.  FIX: sometime, data includes 0 LD at inlet already!!
    L = []
    for x in LD:
        L.append(x * d)
    widthRcritArray = np.shape(Rcrit)[1] + 1
    R = np.zeros((2, widthRcritArray))
    R[:, 0] = [(d / 2.0), 0]
    R[:, 1:widthRcritArray] = Rcrit * np.array(rBound).T * (d / dorig)
    f = interp1d(L, R[0, :])
    lowf = interp1d(L, R[1, :])
    # Integrate by midpt Riemann sum method
    L1, Volume, L_step, end, LD_fine = 0, 0, 0.001, 0, []
    while end != 1:
        try:
            L2 = L1 + L_step
            LD_fine.append(L1)
            r1, r2 = f(L1), f(L2)
            lowr1, lowr2 = lowf(L1), lowf(L2)
            high_vol_slice = (math.pi * ((r1 + r2) / 2) ** 2) * (L_step)
            low_vol_slice = (math.pi * ((lowr1 + lowr2) / 2) ** 2) * (L_step)
            vol_slice = high_vol_slice - low_vol_slice
            Volume = Volume + vol_slice
            L1 = L2
        except:
            end = 1
    dimlessR = R / d  # Non dimensionalize R
    figname = out + "rev_volume_" + str(fig)
    plotDmgVolume(LD, dimlessR, pressure_bounds, figname)
    LD.pop(0)
    return Volume


def buildZOIs(dmgP, Rcrit, LD, jetBounds, breakD, dorig, outdir):
    ''' Compute ZOI revolved volume. Format ZOI contour plot. '''
    plt.subplot2grid((100, 1), (0, 0), rowspan=78)
    rev_vol, fig = [], 0
    matplotlib.colors.BoundaryNorm(dmgP, len(dmgP))
    for i in range(0, len(dmgP)):
        rev_vol.append(dmgVolume(LD, Rcrit[i, :, :], jetBounds, breakD, dorig, [min(dmgP), max(dmgP), dmgP[i]], fig, outdir))
        pl.ylim(0, np.max(Rcrit) * np.max(jetBounds) + 1.0)
    ax2 = plt.subplot2grid((100, 1), (92, 0), rowspan=8)
    cbar = matplotlib.colorbar.ColorbarBase(ax2, orientation='horizontal', format='%.2e', norm=matplotlib.colors.Normalize(vmin=min(dmgP), vmax=max(dmgP)))
    cbar.locator = matplotlib.ticker.MaxNLocator(nbins=6)
    cbar.update_ticks()
    pl.xlabel("Stagnation Presure [Pa]")
    pl.savefig(outdir + "damage_color.png")
    pl.clf()
    pl.cla()
    pl.close()
    return rev_vol


def volEqRadius(rev_vol, dmgP, d):
    ''' Compute volume equivilent spherical radii.

    :param rev_vol: ZOI volumes
    :type rev_vol: array
    :param dmgP: Corresponding damage pressure array
    :type dmgP: array
    :param d: Break diameter
    :type d: float

    :returns:  normalized_vol (array):  Dimensionless ZOI Volume equivilent spherical radii
    '''
    normalized_vol = []
    for i in range(0, len(dmgP)):
        normalized_vol.append(((rev_vol[i] * (
            3.0 / 4.0) * (1.0 / math.pi)) ** (1.0 / 3.0)) * (1.0 / d))
    return normalized_vol


def momSimple(r, coeffs, N):
    return r ** (N + 1) * (coeffs[0] + coeffs[1] * r ** 2 + coeffs[2] * r ** 4)


def momExpFn(r, coeffs, N):
    ''' supply coefficients: coeffs
    r is the evaluation points (dimensionless radial distance) '''
    return r ** (N + 1) * coeffs[0] * (np.exp(coeffs[1] * r ** 2 + coeffs[2] * r ** 4) + coeffs[3])


def momTanhFn(r, coeffs, N):
    ''' combination of hyperbolic tangent functions to make a plateu or square wave-like
    curve.  Subtract a gaussian portion to achive good fit to possible bimodal profiles. '''
    return r ** (N + 1) * (coeffs[0] * (0.5 + 0.5 * np.tanh(coeffs[1] * (r + 1.0))) * \
                           (0.5 + 0.5 * np.tanh(coeffs[1] * (-1.0) * (r - 1.0))) + \
                           coeffs[2] * np.exp(-4.0 * r ** 2) + coeffs[3])


def computeMoments(coeffs, n, targetFcn):
    ''' supply a target function and the fn coefficients
    supply radial integration limits, typically [0, 1]
    supply an integer <n> that denotes largest moment to compute.
    returns moment array '''
    moments = np.zeros(n)
    if targetFcn == 'expharmonic':
        momFcn = momExpFn
    elif targetFcn == 'tanh':
        momFcn = momTanhFn
    elif targetFcn == 'simple':
        momFcn = momSimple
    for N in range(n):
        moments[N] = integrate.quad(momFcn, 0, 1, args=(coeffs, N))[0]
    if math.isnan(sum(moments)) is True:
        print "Nan encountered in moment matching"
        sys.exit()
    return moments


def computeMoments2(coeffs, n, targetFcn):
    moments = np.zeros(len(n))
    if targetFcn == 'expharmonic':
        momFcn = momExpFn
    elif targetFcn == 'tanh':
        momFcn = momTanhFn
    elif targetFcn == 'simple':
        momFcn = momSimple
    for N in n:
        moments[N] = integrate.quad(momFcn, 0, 1, args=(coeffs, N))[0]
    if math.isnan(sum(moments)) is True:
        print "Nan encountered in moment matching"
        sys.exit()
    return moments


def momObjFn(coeffs, moments, n, targetFcn, weights=np.array([])):
    ''' The goal is to minimize this objective function with respect to
    fitting coefficients. This is a weighted least squares problem
    where the weights are equivillent to the inverse of the varience in the
    estimated moments. '''
    if weights.any() and len(weights) != len(moments):
        print("Warning weight array size mismatch.  Setting all weights to 1.")
        weights = np.ones(len(moments))
    elif not weights.any():
        weights = np.ones(len(moments))
    if targetFcn == 'simple':
        # Returns len(moments) size vector for use with leastsq routine
        E = (moments - computeMoments(np.append(coeffs, 0), n, targetFcn))
    else:
        # returns a single L2 norm value for use with minimization package
        # of choice.  Could change obj fn if L2 norm is not ideal
        E = linalg.norm(moments - computeMoments(coeffs, n, targetFcn))
    return E


def minimizeObjFn(moments, guessCoeffs, n, targetFcn='expharmonic', **kwargs):
    ''' Minimization gateway function. Minimizes the target function with respect to fitting coeffs.
    Relies on scipy.optimize.  Also computes the cov matrix for the model parameters so
    that uncertainty can be propogated to the dmg pressure intersection routines. '''
    covCoeffs, withBasinhop, weights = np.zeros((len(guessCoeffs), len(guessCoeffs))), None, np.ones(len(moments))
    for key, value in kwargs.iteritems():
        if key == 'withBasinhop':
            withBasinhop = value
        elif key == 'weights':
            weights = value
    if targetFcn == 'expharmonic':
        # expharmonic constraints and boundaries
        cons = ({'type': 'ineq', 'fun': lambda coeffs: -1.0 * coeffs[1] - 1.0 * coeffs[2]},
                {'type': 'ineq', 'fun': lambda coeffs: -1.0 * coeffs[1] - 2.0 * coeffs[2]})
        bnds = ((0, 1e3), (-2, 10), (-15, 0), (guessCoeffs[3], guessCoeffs[3]))
    elif targetFcn == 'tanh':
        # hyperbolic tangent + exp constraints and boundaries
        cons = ({'type': 'ineq', 'fun': lambda coeffs: coeffs[0]})
        bnds = ((0, 1e3), (2, 60), (-4.5, 2), (guessCoeffs[3], guessCoeffs[3]))
    else:
        cons = ({'type': 'ineq', 'fun': lambda coeffs: coeffs[0]})
        bnds = ((0, 1e3), (-100, 100), (-100, 20), (guessCoeffs[3], guessCoeffs[3]))
    if not withBasinhop and targetFcn != 'simple':
        # local minimization, initial guess betas must be good
        optiCoeffs = minimize(momObjFn, guessCoeffs, args=(moments, n, targetFcn), method='SLSQP', bounds=bnds, constraints=cons, options={'maxiter': 300}).x
    elif targetFcn == 'simple':
        # TODO: cov matrix of model params only availible for this
        # method at this time.  Apply this procedure for all methods here in
        # the future
        # NOTE: The leading constant coefficient in the exponential model
        # profiel Will be (nearly) = 0 since ln(Pjet/Pinf) approaches 0 at the
        # jet boundary.
        optiCoeffs, cov_x, info, msg, its = leastsq(momObjFn, guessCoeffs[:-1], args=(moments, n, targetFcn, weights), full_output=1)
        tempMat = np.eye(len(guessCoeffs))
        tempMat[0:len(cov_x), 0:len(cov_x)] = cov_x
        covCoeffs = np.linalg.inv((1 / weights) * tempMat)
        optiCoeffs = np.append(optiCoeffs, 0.0)
    elif withBasinhop[0] == 1:
        # far more computationally expensive global minimization method
        optiCoeffs = basinhopping(momObjFn, guessCoeffs, niter=50, T=withBasinhop[1], minimizer_kwargs={'method': 'SLSQP', 'args': (moments, n, targetFcn), 'bounds': bnds, 'constraints': cons, 'options': {'maxiter': 300}}).x
    elif withBasinhop[0] == 2:
        # very expensive simulated anneling method
        optiCoeffs = minimize(momObjFn, guessCoeffs, args=(moments, n, targetFcn), method='Anneal', options={'lower': guessCoeffs * -1.5, 'upper': guessCoeffs * 1.5}).x
    return optiCoeffs, covCoeffs
