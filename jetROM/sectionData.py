import numpy as np
import sys
from scipy import integrate
import re
import os
import math


class SectionData(object):
    '''
    Stores pressure distro data at each LD slice.
    Contains methods to compute moments of pressure distributions,
    find the jet boundary.
    '''
    def __init__(self, input_file, tagP0T0, pInf):
        if os.path.exists(input_file) is False:
            sys.exit("Input file path does not exist.")
        self.ld = strip_ld(input_file)
        self.r, self.stagp = get_radial_data(input_file)
        xy_file_path = os.path.dirname(input_file)
        for myfile in os.listdir(xy_file_path):
            if myfile.endswith('.sum'):
                self.sumry_file = myfile
        self.P0, self.T0 = get_bcs(xy_file_path + "/" + self.sumry_file, tagP0T0)
        self.find_jetbound(pInf)

    def find_jetbound(self, pInf):
        self.jetbound, self.stagp, self.r = jet_bounds(self.r, self.stagp, pInf)

    def norm_jet(self, pInf, lnswitch):
        ''' Transform the radi onto interval [0,1] and [-1,1] '''
        self.z, self.rstar = normalize(self.r, self.jetbound)
        if lnswitch:
            # optionally take natural log of all input pressures
            # ln(P/Pinf) = ln(P) - ln(Pinf)
            self.P0 = np.log(self.P0) - np.log(pInf)
            self.stagp = np.log(self.stagp) - np.log(pInf)
        else:
            self.P0 = np.array(self.P0) / pInf
            self.stagp = np.array(self.stagp) / pInf

    def compute_moments(self, max_moment):
        ''' Compute __dimensionless__ pressure profile moments. '''
        self.moments = simps_moments(self.r, self.stagp, max_moment)
        self.zmoments = simps_moments(self.z, self.stagp, max_moment)  # [-1, 1] interval legendre moments
        self.norm_moments = simps_moments(self.rstar, self.stagp, max_moment)  # [0, 1] interval norm moments


###############################################################################
#                           SECTION DATA FN LIBRARY                           #
###############################################################################

def get_bcs(input_file, tagP0T0):
    '''
    Parses boundary condition summary report generated
    by Fluent for P0 and T0.
    '''
    summary_file = open(input_file, "r")
    T0_match = tagP0T0[0]
    P0_match = tagP0T0[1]
    for line in summary_file:
        if re.match(P0_match, line):
            mymatch = P0_match.search(line)
            P0 = (float(mymatch.group(1)))
        if re.match(T0_match, line):
            mymatch = T0_match.search(line)
            T0 = (float(mymatch.group(1)))
    summary_file.close()
    return [P0, T0]


def get_radial_data(in_file):
    '''
    Parses radial distribution .xy files from Fluent.
    '''
    r, stagp = [], []
    input_file = open(in_file, "r")
    match_str = re.compile('([\d.e+-]+)\s+([\d.e+-]+)')
    for current_line in input_file.readlines():
        if match_str.search(current_line):
            match = match_str.search(current_line)
            r.append(float(match.group(1)))
            stagp.append(float(match.group(2)))
    # ensure data is in acending order
    if r[0] > r[2]:
        r.reverse()
        stagp.reverse()
    input_file.close()
    return [r, stagp]


def strip_ld(input_file):
    '''
    Strips a numeric value out of a file path string.
    Returns numeric value imbeded in string.
    '''
    match_str = re.compile('[\d.]+[\d_.]*')
    mymatch = match_str.findall(os.path.basename(input_file))[-1]
    LD = float(mymatch.replace("_", "."))
    return LD


def jet_bounds(r, stagp, pinf):
    ''' Finds the edge of the jet.
    Jet bound must come at r > (r @ a global max)
    TODO: imporve jet boundary detection!'''
    j, blowoff = 0, 0
    maxRindex = stagp.index(max(stagp))
    while blowoff != 1:
        # print stagp[j]
        try:
            if (stagp[j] - pinf) <= 0 and j > maxRindex:  # edge of the jet found!
                sys.stdout.write('.')
                sys.stdout.flush()
                # linear interp for intersection
                delr = r[j] - r[j - 1]
                delp = stagp[j] - stagp[j - 1]
                rcrit = r[j - 1] + (pinf - stagp[j]) * (delr / delp)
                r[j - 1] = rcrit
                stagp[j - 1] = pinf
                stagp = stagp[0:j]  # trim excess p data
                r = r[0:j]  # trim excess r data
                blowoff = 1
            else:
                j = j + 1
        except IndexError:
            # jet boundary not found
            sys.stdout.write('x')
            sys.stdout.flush()
            blowoff = 1
    rbound = max(r)
    if rbound == 0:
        rbound = None
    return [rbound, stagp, r]


def normalize(r, rbound):
    ''' Produces normalized raidii on z[-1,1] and rstar[0,1] '''
    z, r_star = [], []
    for j in range(0, len(r)):
        z.append((r[j] / rbound) * 2 - 1)
        r_star.append(r[j] / rbound)
    return [z, r_star]


def simps_moments(r, stagp, max_moments):
    ''' Compute the moments of the raw pressure profiles
    by simpsons rule integration.  Possible fallback to
    trapazoidal rule integration if err '''
    moments = []
    for n in range(0, max_moments):
        integrand = np.array(stagp) * (np.array(r) ** (n + 1))
        if math.isnan(np.sum(integrand)):
            print("Nan in integrand.  Check input data.")
        moments.append(integrate.simps(integrand, r))
        if math.isnan(moments[-1]) == 1:
            #provides backup trapazoid integration if
            #simpsons rule integration fails
            moments.pop()
            moments.append(integrate.trapz(integrand, r))
        if len(integrand) != len(r):
            print("Warning: Integration vector length mismatch!!")
            print(len(integrand))
            print(len(r))
    return moments
