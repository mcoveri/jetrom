# Test critical module components
# Run from command line:
# python -m unittest unitTests
#

import unittest
import main
import os
import numpy as np
import jetROM as rf


class TestSectionData(unittest.TestCase):
    """
    Tests the SectionData class methods and helper
    functions for anomalies. """
    def test_setUP(self):
        r = np.linspace(0, 1, 50)
        p = 2 * r ** 2
        m = rf.sectionData.simps_moments(r, p, 3)
        self.assertEqual(len(m), 3)
        self.assertAlmostEqual(m[0], 1.0 / 2.0, places=4)
        self.assertAlmostEqual(m[1], 0.4, places=4)


class TestJetData(unittest.TestCase):
    """
    Tests the JetData class methods and helper
    functions for anomalies. """
    def setUP(self):
        pass


class TestJetZOI(unittest.TestCase):
    """
    Tests the SectionData class methods and helper
    functions for anomalies. """
    def setUP(self):
        pass


class KastnerTest(unittest.TestCase):
    """
    Runs Kastern's model through jetROM.  Original ZOIs must agree with
    predicted ZOIs within 2 percent diff.  Good overall check to make sure
    the code runs.
    """
    def runMain(self):
        JetDataInit = {k: main.ds[k] for k in ('rootDir', 'tagP0T0', 'fileTag', 'outdir') if k in main.ds}  # easy way to pull subset dict out of large dict
        mPredict = rf.jetData.JetData(**JetDataInit)  # unpack and pass init inputs
        mPredictOpts = {k: main.ds[k] for k in ('judgeFit', 'tSigLvl', 'method', 'maxM', 'cutoffP', 'ln', 'dataD', 'dimlessL', 'dimlessR', 'setMaxBetaSkip', 'piecewise') if k in main.ds}
        mPredict.setOpts(**mPredictOpts)  # set options, blank for default opts
        mPredict.build_data()  # gather and normalize data
        jetROMOpts = {k: main.ds[k] for k in ('method', 'utilizedM', 'profileFn', 'guess', 'ldMax', 'computeD', 'ldStart', 'ldInc', 'fidelity', 'pickleF', 'extrapolate') if k in main.ds}
        ROM = rf.jetZOI.JetROM(mPredict, rf.modelBank.equationBank, **jetROMOpts)
        jetROMEval = {k: main.ds[k] for k in ('P0', 'T0', 'dmgP', 'outdir') if k in main.ds}
        ROM.constructROM(**jetROMEval)  # This function has only basic inputs - all complex settings set elsewhere
        return ROM

    def test_Kastner(self):
        """"
        Kastner's model is useful to test because Kastner's original pressure
        profiles are gaussian in nature and the reconstruction procedure tested here attempts
        to use gaussian profiles.  Very good agreement must be seen.
        """
        cd = os.getcwd()
        main.inputFile = cd + '/test/unittestin.txt'
        main.ds['rootDir'] = cd + '/test/kastnertest/'
        main.ds['outdir'] = cd + '/test/'
        main.parseInputs(main.inputFile)
        main.makeDir(main.ds['outdir'])
        #main.ds['method'] = 'expharmonic'
        main.ds['method'] = 'simple'
        ROM = self.runMain()
        predictedZOI = ROM.ZOInormvols
        # rerun for original
        main.ds['method'] = 'original'
        ROM = self.runMain()
        originalZOI = ROM.ZOInormvols
        # check for agreement
        print(str(np.array((predictedZOI, originalZOI)).T))
        # fail test if rel err greater than 2%
        self.assertLess(100 * abs(predictedZOI[1] - originalZOI[1]) / originalZOI[1], 2.0)
        self.assertLess(100 * abs(predictedZOI[2] - originalZOI[2]) / originalZOI[2], 2.0)
        self.assertLess(100 * abs(predictedZOI[3] - originalZOI[3]) / originalZOI[3], 2.0)
        self.assertLess(100 * abs(predictedZOI[4] - originalZOI[4]) / originalZOI[4], 2.0)
        self.assertLess(100 * abs(predictedZOI[5] - originalZOI[5]) / originalZOI[5], 2.0)
        self.assertLess(100 * abs(predictedZOI[6] - originalZOI[6]) / originalZOI[6], 2.0)
