#!/usr/bin/python
import numpy as np

#########################################################
#             COMMON FUNCTION LIBRARY                   #
#########################################################

def gradF(targetFn, fnCoords, step=1e-1):
    ''' Compute the partial derivitives of a function
    via central finite diff '''
    delFnCoords = np.eye(len(fnCoords)) * step  # Define step arrays
    delFnCoords2 = np.eye(len(fnCoords)) * 2.0 * step
    nablaQ = map(lambda dC, dC2: ((1.0 / 12.0) * targetFn(fnCoords - dC2) - (2.0 / 3.0) * targetFn(fnCoords - dC) +
                                  (2.0 / 3.0) * targetFn(fnCoords + dC) - (1.0 / 12.0) * targetFn(fnCoords + dC2)) / (step), delFnCoords, delFnCoords2)
    return np.array(nablaQ)
