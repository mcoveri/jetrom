import numpy as np
from math import exp


def defHardPolyFunc(B, coords, args=[]):
    ''' Custom surface function '''
    # Flag critical coeffs that MUST remain in fit. this
    # is to ensure all explanatory vars actually drive the fit.
    # It is possible t-test based elimination will
    # remove coeffs we definately want to keep in the fn!
    criticalBetas = []
    if args:
        skipB = list(set(args).difference(set(criticalBetas)))
        B[skipB] = 0
    response = np.zeros([coords.shape[1]])
    for i in range(0, coords.shape[1]):
        response[i] = B[0] + B[1] * coords[0, i] + B[2] * coords[0, i] * coords[0, i] + B[3] * exp(-0.3 * coords[0, i]) - B[4] * exp(-0.4 * coords[0, i]) + B[5] * coords[2, i] + B[6] * coords[0, i] * coords[1, i] + B[7] * coords[1, i] * coords[2, i] + B[8] * coords[2, i] * coords[0, i] + B[9] * coords[1, i]
    return response


def defHardPolyFunc2(B, coords, args=[]):
    ''' Custom surface function '''
    # Flag critical coeffs that MUST remain in fit. this
    # is to ensure all explanatory vars actually drive the fit.
    # It is possible t-test based elimination will
    # remove coeffs we definately want to keep in the fn!
    criticalBetas = []
    if args:
        skipB = list(set(args).difference(set(criticalBetas)))
        B[skipB] = 0
    response = np.zeros([coords.shape[1]])
    for i in range(0, coords.shape[1]):
        response[i] = B[0] + B[1] * coords[0, i] + B[2] * coords[0, i] * coords[0, i] + B[3] * exp(-0.3 * coords[0, i]) - B[4] * exp(-0.4 * coords[0, i]) + B[5] * coords[2, i] + B[6] * coords[0, i] * coords[1, i] + B[7] * coords[1, i]
    return response


def defHardPolyFunc3(B, coords, args=[]):
    ''' Custom surface function '''
    # Flag critical coeffs that MUST remain in fit. this
    # is to ensure all explanatory vars actually drive the fit.
    # It is possible t-test based elimination will
    # remove coeffs we definately want to keep in the fn!
    criticalBetas = []
    if args:
        skipB = list(set(args).difference(set(criticalBetas)))
        B[skipB] = 0
    response = np.zeros([coords.shape[1]])
    for i in range(0, coords.shape[1]):
        response[i] = B[0] + B[1] * coords[0, i] + B[2] * coords[0, i] * coords[0, i] + B[3] * coords[2, i] + B[4] * coords[0, i] * coords[1, i] + B[5] * coords[1, i] * coords[2, i] + B[6] * coords[2, i] * coords[0, i] + B[7] * coords[1, i]
    return response

'''
This list holds all the functions that will be tried for
surface fitting.  Also, the initial guess for the fitting
coefficients must be specifed alongside the function.
'''
equationBank = [[[defHardPolyFunc, np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1])], [defHardPolyFunc2, np.array([1, 1, 1, 1, 1, 1, 1, 1])]],
                [[defHardPolyFunc, np.array([1, 1, 1, 1, 1, 1, 1, 1, 1, 1])], [defHardPolyFunc3, np.array([1, 1, 1, 1, 1, 1, 1, 1])]]]
