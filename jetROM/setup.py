# To use this script execute either
# python setup.py install
# or
# python setup.py develop
# You may need to be root

from setuptools import setup, find_packages
setup(
    name = "jetROM",
    version = "0.1",
    packages = find_packages(),
    #scripts = ['main.py'],
    install_requires = ['numpy>=1.7.0', 'scipy>=0.12.0', 'matplotlib>=1.2.1'],
    package_data = { '': ['*.txt'] },
    author = 'William Gurecky',
    license = "BSD",
    author_email = "william.gurecky@gmail.com",

    # set primary console script up
    entry_points = {
        'console_scripts': [
            'runjetROM = jetROM.main:main'
        ]
    }
)

