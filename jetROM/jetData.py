import numpy as np
import multiprocessing
import itertools
import os
import sys
import scipy
import collections
import re
import sectionData as sd
from scipy.odr import odrpack as odr
from scipy import stats
import functools


class JetData(object):
    '''
    Stores all jet data - every LD slice.
    Contains surface regression methods.
    '''
    def __init__(self, rootDir, tagP0T0, fileTag="ld_", outdir='./test/'):
        '''
        Searches top-down in master directory tree for .xy files.
        Determins what type of dataset (free or impinging) is
        being used.
        '''
        self.tagP0T0 = tagP0T0
        self.outdir = outdir
        self.xy_file_list = find_files(fileTag, rootDir)
        self.jet_type = data_source_type(self.xy_file_list)
        self.surface_fits = []

    def build_data(self):
        ''' Aggregate ld_sections: store many instances of
        radial jet data taken at discrete LD '''
        if not hasattr(self, 'lnswitch'):
            self.setOpts()  # if user forgot to set options, still run with defaults
            print('Warning: Please set options for moment prediction. Defaults used.')
        self.ld_section = []
        for xy_file in self.xy_file_list:
            self.ld_section.append(sd.SectionData(xy_file, self.tagP0T0, self.pInf))
            # Do not accept faulty profile (could not find jet bound) or ld = 0
            # (not physically meaningful)
            if self.ld_section[-1].jetbound is None or self.ld_section[-1].ld == 0:
                sys.stdout.write('x')
                sys.stdout.flush()
                self.ld_section.pop()
        self.norm_data()  # organize data into tree struct and in np arrays

    def norm_data(self):
        ''' Collect all data into numpy arrays.
        Construct DataTree, which has the following structure:
              /      /        /
        (root)-----(LD)-----(P0)-------(T0)[Data leaf]
              \      \        \
        Normalize data if given dimensioned data.
        Pnorm = P/Pinf,  Rnorm = R/D,  Lnorm = L/D
        The moments become dimensionless if normalized data used
        '''
        self.DataTree = DataTree('root')  # initialize data tree
        self.jetP0 = np.zeros(len(self.ld_section))  # initialize np arrays
        self.jetT0 = np.zeros(len(self.ld_section))
        self.jetLD = np.zeros(len(self.ld_section))
        self.jetB = np.zeros(len(self.ld_section))
        self.jetM = np.zeros((len(self.ld_section), self.maxMoment))
        self.jetMz = np.zeros((len(self.ld_section), self.maxMoment))
        self.jetMrs = np.zeros((len(self.ld_section), self.maxMoment))
        #
        # Normalization.  Always non-dimensionalize distances by break diam
        # If user already supplied dimless distances do nothing
        self.normR = self.Doriginal if not self.dimlessR else 1.0
        self.normL = self.Doriginal if not self.dimlessL else 1.0
        #
        print('Computing Moments...')
        for i, section in enumerate(self.ld_section):
            section.norm_jet(self.pInf, self.lnswitch)
            section.compute_moments(self.maxMoment)
            self.jetP0[i] = section.P0
            self.jetT0[i] = section.T0
            self.jetLD[i] = section.ld / self.normL
            self.jetB[i] = section.jetbound / self.normR
            self.jetM[i, :] = np.array(section.moments)  # each row is LD slice
            self.jetMz[i, :] = np.array(section.zmoments)
            self.jetMrs[i, :] = np.array(section.norm_moments)
            self.DataTree.addMNodes(['root', self.jetLD[i], self.jetP0[i], self.jetT0[i]], [
                np.array(section.zmoments), np.array(section.norm_moments),
                np.array(section.stagp), np.array(section.z), np.array(section.rstar)])
        self.ldMax = max(self.jetLD)
        self.ld_section = []  # clean up, no need to retain this info

    def checkOpts(self):
        ''' Check for incorrect inputs and exit if an error is found '''
        try:
            np.sum([self.tSigLvl, self.pInf, self.Doriginal, self.maxMoment])
        except:
            sys.exit('String detected in numeric input field. Exiting.')
        if self.maxMoment < 2:
            sys.exit('The maximum number of predicted moments to compute is required to be greater than 1')
        # TODO: add more checks

    def setOpts(self, **kwargs):
        ''' User input dictionary of surface fitting settings. Provide sane
        defaults if field is not populated. '''
        self.fitCriterion = kwargs.pop('judgeFit', 'AIC')
        self.tSigLvl = kwargs.pop('tSigLvl', 0.10)
        self.momentType = kwargs.pop('method', 'legendre')  # standard or legendre
        self.maxMoment = kwargs.pop('maxM', 4)  # maximum number of moments to predict
        self.pInf = kwargs.pop('cutoffP', 2.05e5)  # effective cutoff pressure, or 'ambient' pressure
        self.lnswitch = kwargs.pop('ln', 1)  # take natural log of Pressures 1 == yes
        self.Doriginal = kwargs.pop('dataD', 0.156)  # source data break diameter
        self.dimlessL = kwargs.pop('dimlessL', True)  # source data dimenssionless L coord?
        self.dimlessR = kwargs.pop('dimlessR', False)  # source data dimensionless R coord?
        self.setMaxBetaSkip = kwargs.pop('setMaxBetaSkip', 3)  # maximum allowable number of fitting coeffs to remove via t-test
        self.lowShockWindow = kwargs.pop('lowLDShock', 2.0)
        self.highShockWindow = kwargs.pop('highLDShock', 15.0)
        self.piecewise = kwargs.pop('piecewise', True)
        self.checkOpts()

    def surfaceTtest(self, surfaceGroup, motherFn, currentFcn, maxBetaSkip):
        ''' Each entry in surfaceGroup contains the fitting parameters
        and fitting statistics for a surface regression <m_i> = <m_i>(P0, T0, LD)
        '''
        treducedFns = []  # stores functions that have some terms elinated by t-test
        for surface in surfaceGroup:
            # conduct students t-test on each coefficinet in fit
            # surface[0]: stores fit info [fitresults, fitcoeffs, ect...]
            # surface[1]: stores fit statistics
            sortedFlaggedBetas = sorted([(tScore, tIndex) for tIndex, tScore in enumerate(surface[1]['critT']) if tScore < 0])
            flaggedBetas = [item[1] for item in sortedFlaggedBetas]
            if flaggedBetas and maxBetaSkip < self.setMaxBetaSkip and maxBetaSkip <= len(flaggedBetas):
                # force coeffs that failed the t-test to 0, fork child from parent
                # treducedFns.append([lambda beta, x, flgB=flaggedBetas[0:maxBetaSkip]: motherFn(beta, x, flgB), currentFcn[1], maxBetaSkip + 1])
                treducedFns.append([functools.partial(motherFn, args=flaggedBetas[0:maxBetaSkip]), currentFcn[1], maxBetaSkip + 1])
        return treducedFns

    def multiFit(self, modelFns, maxMoment, zone):
        '''
        Attempts regression with all the functions specified in the modelFn
        bank.  Also attempts to eliminate unsignificant terms, then refit.
        Returns the "best fit" function after comparing all the possibilities.
        '''
        # zone - nearfield or farfield zone
        # grp - every surface group is tied to a parent model def by user in
        # functionBank input file.
        # surf - momentPredictionSurface
        pool = multiprocessing.Pool()
        mom = range(maxMoment)
        motherFn = []
        print('Surface Fitting...')
        while modelFns:
            fcn = modelFns.pop()
            try:
                maxBetaSkip = fcn[2]  # fcn will not contain 3rd element unless unignificant terms are tagged
            except IndexError:
                maxBetaSkip = 1
                motherFn = fcn[0]
            # fcn[0]: function,  fcn[1]: fitcoeffs (beta), fcn[2]: skipped betas
            explanVars = np.array([self.tempJetLD, self.tempJetP0, self.tempJetT0])
            surfaceGroup = pool.map(functools.partial(fitSurface, modelFn=fcn[0], initBeta=fcn[1], explanVars=explanVars,
                                                      tempJetMom=self.tempJetMom, tSigLvl=self.tSigLvl, nfailedParams=(maxBetaSkip - 1)), mom)  # Compute Fits
            self.surfaceBucket[zone].append(surfaceGroup)
            self.modelFnBucket[zone].append(fcn[0])
            modelFns = modelFns + self.surfaceTtest(surfaceGroup, motherFn, fcn, maxBetaSkip)  # append function(s) with reduced degs of freedom
            sys.stdout.write('.')
            sys.stdout.flush()
        print('Surface Fitting Complete')
        Gscore = np.zeros((len(self.surfaceBucket[zone]), len(mom)))
        # Obtain quality of fit statistic for all fits in all zones for all
        # moments, store as 'Gscore' - not related to any true stat
        for grp, surfaceGroup in enumerate(self.surfaceBucket[zone]):
            for surf, surface in enumerate(surfaceGroup):
                if self.fitCriterion == 'AIC':
                    Gscore[grp, surf] = -self.surfaceBucket[zone][grp][surf][1]['AICc']
                elif self.fitCriterion == 'BIC':
                    Gscore[grp, surf] = -self.surfaceBucket[zone][grp][surf][1]['BIC']
                elif self.fitCriterion == 'Fratio':
                    Gscore[grp, surf] = self.surfaceBucket[zone][grp][surf][1]['Fratio']
                elif self.fitCriterion == 'Rsqrd':
                    Gscore[grp, surf] = self.surfaceBucket[zone][grp][surf][1]['Rsqrd']
                self.plotFitStats(zone, grp, surf)
        # cherry pick the functions with the best Gscore
        victorSurfaceFits, victorModelFns = [], []
        for m in mom:
            winGrpIndex = np.argmax(Gscore[:, m])
            # store the winning model coefficients and fit stats
            victorSurfaceFits.append(self.surfaceBucket[zone][winGrpIndex][m])
            # Also need to store the corresponding winning model functions
            victorModelFns.append(self.modelFnBucket[zone][winGrpIndex])
            self.plotFitStats(zone, winGrpIndex, m, explanVars)
        self.surfaceBucket = [[], []]
        self.modelFnBucket = [[], []]
        return [victorSurfaceFits, victorModelFns]

    def plotFitStats(self, zone, grp, surf, logWinnerFlag=None):
        with open(self.outdir + 'surfacefitLog.txt', 'a') as outFile:
            if logWinnerFlag is not None:
                outFile.write('************** VICTORIOUS FIT ***************\n')
            outFile.write('zone (0-nearfield, 1-farfield): ' + str(zone) + ' group: ' + str(grp) + '  moment:' + str(surf) + '\n')
            outFile.write(' ==== General-statistics ==== \n')
            outFile.write('beta: ' + str(self.surfaceBucket[zone][grp][surf][0].beta) + '\n')
            outFile.write('SE beta: ' + str(self.surfaceBucket[zone][grp][surf][0].sd_beta) + '\n')
            outFile.write('Covar beta: ' + str(self.surfaceBucket[zone][grp][surf][0].cov_beta) + '\n')
            Rsqrd = self.surfaceBucket[zone][grp][surf][1]['Rsqrd']
            AICc = self.surfaceBucket[zone][grp][surf][1]['AICc']
            BIC = self.surfaceBucket[zone][grp][surf][1]['BIC']
            chiSqrd = self.surfaceBucket[zone][grp][surf][1]['chiSqrd']
            Fratio = self.surfaceBucket[zone][grp][surf][1]['Fratio']
            critChi = self.surfaceBucket[zone][grp][surf][1]['critChi']
            alphaChi = self.surfaceBucket[zone][grp][surf][1]['chiSig']  # siglvl chi2 test (alpha)
            outFile.write('R^2: ' + str(Rsqrd) + '\n')
            outFile.write('ChiSqrd: ' + str(chiSqrd) + '\n')
            outFile.write('CritChi: ' + str(critChi) + '   siglvl (alpha): ' + str(alphaChi) + '\n')
            outFile.write('Fratio: ' + str(Fratio) + '\n')
            outFile.write('AICc: ' + str(AICc) + '\n')
            outFile.write('BIC: ' + str(BIC) + '\n')
            outFile.write(' ==== T-statistics ==== \n')
            tstat = self.surfaceBucket[zone][grp][surf][1]['t_stat']  # t
            tscores = self.surfaceBucket[zone][grp][surf][1]['critT']  # t - tcrit
            CI = self.surfaceBucket[zone][grp][surf][1]['confI']  # +/- SE * t
            alphaT = self.surfaceBucket[zone][grp][surf][1]['sigT']  # siglvl t test (alpha)
            outFile.write('T statistic (B/SE(B)): ' + str(tstat) + '\n')
            outFile.write('T scores (|t| - tcrit): ' + str(tscores) + '   siglvl (alpha): ' + str(alphaT) + '\n')
            outFile.write("Confidence interval, B_i +/-: " + str(CI) + '\n')
            outFile.write('--------------------------------------------------- \n')
        if logWinnerFlag is not None:
            resid = self.surfaceBucket[zone][grp][surf][1]['resid']
            np.savez(self.outdir + 'residlog_' + str(surf) + '.npz', logWinnerFlag[0],
                     logWinnerFlag[1], logWinnerFlag[2], resid / abs(self.tempJetMom[:, surf]))

    def __gradgradF(self, momVsLD):
        ''' take second derivitive '''
        centeredX = momVsLD[0][1:-1]
        backX = momVsLD[0][0:-2]
        forwardX = momVsLD[0][2:]
        backDeltaX = centeredX - backX
        forwardDeltaX = forwardX - centeredX
        yCentered = momVsLD[1][1:-1]
        yBack = momVsLD[1][0:-2]
        yForward = momVsLD[1][2:]
        d2M = 2 * (backDeltaX * yForward - (backDeltaX + forwardDeltaX) *
                   yCentered + forwardDeltaX * yBack) / (backDeltaX * forwardDeltaX * (backDeltaX + forwardDeltaX))
        # scipy alternative, much cleaner.  In testing.
        lowcut = np.argmin(np.abs(self.lowShockWindow - centeredX))
        highcut = np.argmin(np.abs(self.highShockWindow - centeredX))
        centeredX = centeredX[lowcut:highcut]
        d2M = d2M[lowcut:highcut]
        return d2M, centeredX

    def breakPoint(self):
        # construct M vs LD curve
        allData = self.DataTree.node['root'].node
        momVsLD = np.zeros((2, len(allData)))
        for i, ldKey in enumerate(dict.keys(allData)):
            LDdata = self.DataTree.getMData(['root', ldKey], [4, 0, 0, 'depth'])
            momentData = []
            for LD in LDdata:
                # *investigates the 0th moment vs Ld relationship only*
                momentData.append(LD[4][self.responseSwitch][0])
            momentData = np.array(momentData)
            avgMomLD = np.average(momentData)
            momVsLD[0, i] = ldKey
            momVsLD[1, i] = avgMomLD
        momVsLD = momVsLD[:, np.argsort(momVsLD[0])]
        d2M_dLD2, centeredX = self.__gradgradF(momVsLD)
        # break at largest, negative second deriv
        minIndex = np.argmin(d2M_dLD2)
        critd2M_dLD2 = -0.4  # if the spike is not sharp enough, do nothing
        if d2M_dLD2[minIndex] < critd2M_dLD2 and len(momVsLD[0]) > 30 and self.piecewise:
            breakPt = centeredX[minIndex]
        else:
            breakPt = None
        return breakPt

    def makeSurfaces(self, requestedMethod, maxMoment, equationBank):
        '''
        Top level function that calls surface building routines
        '''
        self.momentType = requestedMethod
        if self.momentType == 'legendre':
            self.responseSwitch = 0
        else:
            self.responseSwitch = 1
        self.surfaceBucket = [[], []]
        self.modelFnBucket = [[], []]
        self.breakPt = self.breakPoint()
        self.pieceWiseFits, self.modelFns = [], []
        pieceWiseDat = []
        if self.breakPt is not None:
            # Currently support only one discontinuity in fit.  TODO: add support for
            # any number of discontinuities in the piecewise regression.
            pieceWiseDat.append(self.DataTree.getMData(['root'], [4, 1, self.breakPt, 'lt']))
            pieceWiseDat.append(self.DataTree.getMData(['root'], [4, 1, self.breakPt, 'gt']))
        else:
            pieceWiseDat.append(self.DataTree.getMData(['root'], [4, 0, 0, 'depth']))
        for zone, pWD in enumerate(pieceWiseDat):
            # zone indicates if we are near field (zone = 0) or far field
            # (zone = 1)
            self.tempJetLD = np.zeros(len(pWD))
            self.tempJetP0 = np.zeros(len(pWD))
            self.tempJetT0 = np.zeros(len(pWD))
            self.tempJetMom = np.zeros((len(pWD), maxMoment))
            for i, LDslice in enumerate(pWD):
                # Build numpy arrays from DataTree
                self.tempJetLD[i] = LDslice[1]
                self.tempJetP0[i] = LDslice[2]
                self.tempJetT0[i] = LDslice[3]
                self.tempJetMom[i] = LDslice[4][self.responseSwitch]
            fitResults = self.multiFit(equationBank[zone], maxMoment, zone)
            self.pieceWiseFits.append(fitResults[0])
            self.modelFns.append(fitResults[1])

    def evalMultiFit(self, queryLDs, queryP0, queryT0, modelFns, surfaceFits):
        '''
        Evaluates the fitted surfaces at the requested LDs,
        P0 and T0.  Returns an array of moments and an estimate
        the uncertainty of each moment.
        '''
        evaluatedMoments = np.zeros((len(queryLDs), self.maxMoment))
        evalMomentsSigma = np.zeros((len(queryLDs), self.maxMoment))
        for i, LDi in enumerate(queryLDs):
            evalPts = np.array([[LDi, queryP0, queryT0]]).T
            evaluatedMoments[i] = np.array([float(modelFns[mom](surfaceFits[mom][0].beta, evalPts)) for mom in range(self.maxMoment)])
            #
            # Compute moment uncertainties by gradF * covM * gradF.T
            mSigma = []
            for mom in range(self.maxMoment):
                targetFn = lambda betas: modelFns[mom](betas, evalPts)
                gF = gradF(targetFn, surfaceFits[mom][0].beta)
                covM = surfaceFits[mom][0].cov_beta
                mSigma.append(np.dot(np.dot(gF.T, covM), gF))
            evalMomentsSigma[i] = np.array(mSigma).T
        return evaluatedMoments.T, evalMomentsSigma.T

    def evalSurfaces(self, LDs, queryP0, queryT0):
        '''
        Evaluates the moment regressions (fitted surfaces) and provides an
        estimate for evaluated moment sigma.
        If the surface is specifed as piecewise, interpolate at discontinuity
        to eliminate any jumps in the moments and the pressure contours.
        '''
        evalMoments = []
        # split the LDs np array at the break point
        for zone, pieceWiseFit in enumerate(self.pieceWiseFits):
            if zone == 0:
                evalMoments, evalSigma = self.evalMultiFit(LDs, queryP0, queryT0, self.modelFns[zone], pieceWiseFit)
            else:
                # blend surface fits at discontinuity
                blendingFn1 = 0.5 + 0.5 * np.tanh(10 * (LDs - self.breakPt))
                evalMomentsFarField, evalSigmaFarField = self.evalMultiFit(LDs, queryP0, queryT0, self.modelFns[zone], pieceWiseFit)
                evalMoments = (1 - blendingFn1) * evalMoments + blendingFn1 * evalMomentsFarField
                evalSigma = (1 - blendingFn1) * evalSigma + blendingFn1 * evalSigmaFarField
        return evalMoments.T, evalSigma.T

    def buildRInterp(self):
        ''' Builds the jet radius Interpolant '''
        points = np.array([self.jetLD, self.jetP0, self.jetT0]).T
        ujetP0 = np.unique(self.jetP0)
        ujetT0 = np.unique(self.jetT0)
        p = np.array(list(itertools.product(*[ujetP0, ujetT0])))
        extrapPoints = np.array([np.zeros(len(p)), p[:, 0], p[:, 1]]).T
        points = np.vstack((extrapPoints, points))
        print np.array([self.jetLD, self.jetP0, self.jetT0]).T.shape
        if np.isnan(np.sum(self.jetT0)) or np.isnan(np.sum(self.jetB)) \
           or np.isnan(np.sum(self.jetP0)) or np.isnan(np.sum(self.jetLD)):
            print('warning. Nan found in interpolent!')
        extrapR = np.ones(len(p)) / 2.0
        jetB = np.append(extrapR, self.jetB)
        self.jetRInterp = scipy.interpolate.LinearNDInterpolator(
            points, jetB, fill_value=0.0)


class DataTree(object):
    ''' The tree is just a container class for many nodes (dictionaries).
    Provides methods to interact with the nodes in the tree so
    that the data can be transformed back to sorted numpy
    arrays. TODO: add delete node method '''
    def __init__(self, key, data=None):
        if key is None and data is None:
            self.node = {}
        elif data is None and key is not None:
            # If no data is supplied, then a make a new node storing a blank
            # Tree class.  This node is accessible by the key
            try:
                self.node.update({key: DataTree(None)})
            except:
                self.node = {key: DataTree(None)}
        elif key is not None and data is not None:
            # If data is supplied in conjuction with a key, create a "leaf"
            # This is essentially a dead end in the tree as
            # the climbTree function will not travel INTO
            # non-Tree class paths
            self.node[key] = data

    def climbTree(self, mother, keyList, depth=0):
        ''' Search the tree for the requested node - as specified by keyList '''
        if isinstance(mother.node[keyList[depth]], DataTree) and depth < len(keyList) - 1:
            return self.climbTree(mother.node[keyList[depth]], keyList[1:], depth)
        else:
            return mother.node[keyList[depth]]

    def getMData(self, startNode, condition=None):
        ''' Retrives all the keys and data underneth the given node.
        If supplied, selects nodes that meet the condition.
        Breadth first search. '''
        outList = []
        queue = [startNode]
        checkedNode = 1
        while queue:
            t = queue.pop(0)
            if condition is None:
                outList.append(t)
            else:
                checkedNode = self.nodeCheck(t, condition)
                if checkedNode:
                    outList.append(checkedNode)
            try:
                attKeys = self.getKeys(t)
            except:
                if checkedNode:
                    outList[-1] = outList[-1] + [self.getData(t)]
                attKeys = []
            for aK in attKeys:
                nextNode = t + [aK]
                queue.append(nextNode)
        return outList

    def nodeCheck(self, node, condition):
        ''' Hackish function, may be modified to suit needs.
        checks node against a set of conditions, passes node through
        if it meets all the criteria, otherwise, return null. '''
        if condition[3] == 'lt' and len(node) == condition[0] and node[condition[1]] < condition[2]:
            pass
        elif condition[3] == 'gt' and len(node) == condition[0] and node[condition[1]] >= condition[2]:
            pass
        elif condition[3] == 'depth' and len(node) == condition[0]:
            pass
        else:
            node = None
        return node

    def addSNode(self, parents, data=None):
        ''' Add one node to the tree.  If data is given, the node
        becomes a "leaf" which holds information '''
        targetNode = self.climbTree(self, parents[:-1])
        if data is None:
            targetNode.__init__(parents[-1])  # add a new node
        else:
            targetNode.__init__(parents[-1], data)  # populate leaf with data

    def addMNodes(self, mParents, data=None):
        ''' Allows multiple nodes to be created at once '''
        root = [mParents[0]]
        parents = mParents[1:]
        nodeIndex = range(1, len(mParents) + 1)
        for i in nodeIndex:
            # Only create node if node does not already exist,
            # dont want to overwrite nodes!
            # If the node already exists we just traverse down
            if self.getNode(root + parents[:i] + ['temp']) is not None:
                nodeExistsAlready = True
            else:
                nodeExistsAlready = False
            if i == len(mParents):
                self.addSNode(root + parents[:i], data)
            elif nodeExistsAlready is False:
                self.addSNode(root + parents[:i])

    def getNode(self, parents):
        ''' Returns the requested node '''
        try:
            targetNode = self.climbTree(self, parents[:-1])
        except:
            targetNode = None
        return targetNode

    def getKeys(self, parents):
        targetKeys = self.getNode(parents + ['temp']).node.keys()
        return targetKeys

    def getData(self, parents):
        targetData = self.getNode(parents).node[parents[-1]]
        return targetData


###############################################################################
#                           JET DATA FN LIBRARY                               #
###############################################################################

def odrfit(model_fn, initBeta, explan_vars, response_var, tSigLvl, nfailedParams):
    ''' Utilizes the orthogonal distance regression toolkit
    provided by sci-py to fit N-dimensional surface to
    moments.
    The moments are a fn of upstream conditions and L/D.
    beta holds the fitting coeffs in the model_fn.
    '''
    my_model = odr.Model(model_fn)
    my_data = odr.Data(explan_vars, response_var)
    my_odr = odr.ODR(my_data, my_model, beta0=initBeta)
    my_odr.set_job(fit_type=2)
    fit = my_odr.run()
    n = explan_vars.shape[1]
    k = explan_vars.shape[0] - nfailedParams
    Rsqrd = 1 - fit.res_var
    # Compute T statistics
    t_stat = fit.beta / fit.sd_beta
    whereNan = np.isnan(t_stat)
    t_stat[whereNan] = 0
    DoF = n - (k + 1)
    critT = stats.t.isf(tSigLvl, DoF)  # t inverse survival fn
    CI = fit.sd_beta * critT  # Conf Interval list: Beta_i +/- CI_i
    # Compute Chi-Sqrd Stats
    sigLvlChi = 0.10
    chiSqrd = n * np.log(fit.sum_square_eps / n)
    critChi = stats.chi2.isf(sigLvlChi, DoF)  # chi inverse survival fn
    # Compute Fratio
    R2 = 1 - fit.res_var
    Fratio = (R2 / n) / ((1 - R2) / (DoF))
    # Compute AIC and BIC
    AICc = n * np.log(fit.sum_square_eps / n) + 2 * k + (2 * k * (k + 1)) / (n - k - 1)
    BIC = n * np.log(fit.sum_square_eps / n) + k * np.long(n)
    # Package fit results
    fit_stats = {'t_stat': t_stat, 'critT': abs(t_stat) - critT,
                 'chiSqrd': chiSqrd, 'critChi': critChi, 'confI': CI,
                 'sigT': tSigLvl, 'chiSig': sigLvlChi, 'Fratio': Fratio,
                 'Rsqrd': Rsqrd, 'AICc': AICc, 'BIC': BIC, 'resid': fit.eps}
    return [fit, fit_stats]


def fitSurface(m, modelFn, initBeta, explanVars, tempJetMom, tSigLvl, nfailedParams):
    '''
    fit a given model to the moments using upstream conditions
    as explanatory variables.  This is located outside of DataClass
    because pickle requres top-level module to serialize?
    '''
    responseVars = tempJetMom[:, m]
    return odrfit(modelFn, initBeta, explanVars, responseVars, tSigLvl, nfailedParams)


def data_source_type(matched_dirs):
    '''
    Determines if each radial distribution xy file is
    in a unique directory.
    if true - Impinging jet case
    else - Free jet case
    '''
    result = []
    for filename in matched_dirs:
        result.append(os.path.dirname(filename))
    counted_list = collections.Counter(result)
    unique_list = [i for i in counted_list if counted_list[i] == 1]
    if len(list(unique_list)) > 1:
        print("Impinging jet case")
        return 1
    else:
        print("Free jet case")
        return 0


def find_files(pattern, path):
    '''
    Finds and retuns the location of all files
    with extention <pattern> in a given directory
    and all of its subdirectories.
    '''
    result = []
    for root, dirs, files in os.walk(path):
        for name in files:
            # if fnmatch.fnmatch(name, pattern):
            if re.search(pattern, name):
                result.append(os.path.join(root, name))
    return result


def gradF(targetFn, fnCoords, step=1e-1):
    ''' Compute the partial derivitives of a function
    via central finite diff '''
    delFnCoords = np.eye(len(fnCoords)) * step  # Define step arrays
    delFnCoords2 = np.eye(len(fnCoords)) * 2.0 * step
    nablaQ = map(lambda dC, dC2: ((1.0 / 12.0) * targetFn(fnCoords - dC2) - (2.0 / 3.0) * targetFn(fnCoords - dC) +
                                  (2.0 / 3.0) * targetFn(fnCoords + dC) - (1.0 / 12.0) * targetFn(fnCoords + dC2)) / (step), delFnCoords, delFnCoords2)
    return np.array(nablaQ)
