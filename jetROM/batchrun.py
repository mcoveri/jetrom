import numpy as np
from scipy.interpolate import griddata
import matplotlib.pyplot as plt
import pylab as pl
import itertools
import subprocess
import warnings


warnings.simplefilter("ignore")
inputFile = 'kastnerinput.txt'
outpath = './batchtest/'
P0array = np.arange(5e6, 8e6, 1e6)
T0array = np.arange(520, 560, 10)
# run jet model for all combinations of above conditions
# store resulting zoi / dmgP data
mash = np.array([P0array, T0array])
paramlist = list(itertools.product(*mash))
ZOI, dmgP, P0, T0 = [], [], [], []
for P0T0 in paramlist:
    try:
        subprocess.call(['python -m jetROM.main', '-i', inputFile, '-o', outpath + str(P0T0[0]) + '_' + str(P0T0[1]) + '/', '-P', str(P0T0[0]), '-T', str(P0T0[1])])
        zoiData = np.load(outpath + str(P0T0[0]) + '_' + str(P0T0[1]) + '/' + 'ZOI_log.npz')
        ZOI.append(zoiData['arr_0'])
        dmgP.append(zoiData['arr_1'])
        P0.append(P0T0[0])
        T0.append(P0T0[1])
    except:
        print('run ' + str(P0T0) + ' failed.  Likely out of range')
# Plot ZOI vs (P0, T0) heatmap
for i, dP in enumerate(dmgP[0]):
    x, y, z = [], [], []
    for j, pt in enumerate(zip(P0, T0)):
        x.append(pt[0])
        y.append(pt[1])
        z.append(ZOI[j][i])
    isoP = str(dP)
    x = np.array(x)
    y = np.array(y)
    z = np.array(z)
    xi = np.linspace(min(x), max(x), 100)
    yi = np.linspace(min(y), max(y), 100)
    X, Y = np.meshgrid(xi, yi)
    Z = griddata((x, y), z, (X, Y), method='linear', fill_value=0.0)
    try:
        levels = np.arange(np.min(Z), np.max(Z) + 0.1, 0.1)
        pl.figure(i)
        CS = pl.contourf(X, Y, Z, levels)
        CB = pl.colorbar(format='%2.1f')
        CB.set_label('ZOI Volume [Break Diameters]')
        CCS = pl.contour(X, Y, Z, levels, colors='k')
        pl.clabel(CCS, inline=1, fontsize=10)
        pl.title('ZOI Volume Map for Damage Pressure = ' + '%.2e' % float(isoP) + '[Pa]')
        pl.xlabel('Upstream Stagnation Pressure [Pa]')
        pl.ylabel('Upstream Stagnation Temperature [K]')
        plt.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
        pl.savefig(outpath + isoP + '.png', dpi=120)
    except:
        print('Plotting err.')
